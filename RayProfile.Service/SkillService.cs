﻿using RayProfile.Data;
using RayProfile.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace RayProfile.Service
{
    public interface ISkillService
    {
        /// <summary>
        /// Add skill to position
        /// </summary>
        /// <param name="skill">The skill to be added</param>
        /// <param name="positionId">The id of a position</param>
        /// <param name="value">The value of the skill</param>
        void AddToPosition(Skill skill, int positionId, int value);

        /// <summary>
        /// Add skill to user
        /// </summary>
        /// <param name="skill">The skill to be added</param>
        /// <param name="userId">The id of a user</param>
        /// <param name="value">The value of the skill</param>
        void AddToUser(Skill skill, int userId, int value);

        /// <summary>
        /// Check id a skill belongs to user
        /// </summary>
        /// <param name="skillId">The id of a skill</param>
        /// <param name="userId">The id of a user</param>
        /// <returns></returns>
        bool BelongsTo(int skillId, int userId);

        /// <summary>
        /// Deletes skills for position
        /// </summary>
        /// <param name="positionId">The id of a position</param>
        void DeleteForPosition(int positionId);

        /// <summary>
        /// Get user skill by id
        /// </summary>
        /// <param name="id">The id of a user skill</param>
        /// <returns></returns>
        UserSkill Get(int id);

        /// <summary>
        /// Gets all skills
        /// </summary>
        /// <returns></returns>
        List<Skill> GetAll();

        /// <summary>
        /// Gets all skills for a user
        /// </summary>
        /// <param name="userId">The id of a user</param>
        /// <returns></returns>
        List<UserSkill> GetByUserId(int userId);

        /// <summary>
        /// Get paged user skills by user id
        /// </summary>
        /// <param name="userId">The id of a user</param>
        /// <param name="page">The page</param>
        /// <param name="pageSize">The size of the page</param>
        /// <returns></returns>
        List<UserSkill> GetPagedByUserId(int userId, int page, int pageSize);

        /// <summary>
        /// Get the most required positions
        /// </summary>
        /// <returns></returns>
        List<Skill> MostUsedInPositions();

        /// <summary>
        /// Remove skill from user
        /// </summary>
        /// <param name="skill">The skill to be removed</param>
        void RemoveFromUser(UserSkill skill);
    }

    public class SkillService : ISkillService
    {
        private AppContext db = new AppContext();

        public void AddToPosition(Skill skill, int positionId, int value)
        {
            if (db.Skills.Count(x => x.Name == skill.Name) == 0)
            {
                db.Skills.Add(new Skill
                {
                    Name = skill.Name,
                });
                db.SaveChanges();
            }
            var skillToBeAdded = db.Skills.First(x => x.Name == skill.Name);
            db.PositionSkills.Add(new PositionSkill
            {
                SkillId = skillToBeAdded.Id,
                PositionId = positionId,
                Value = value,
            });
            db.SaveChanges();
        }

        public void AddToUser(Skill skill, int userId, int value)
        {
            if (db.Skills.Count(x => x.Name == skill.Name) == 0)
            {
                db.Skills.Add(new Skill
                {
                    Name = skill.Name,
                });
                db.SaveChanges();
            }
            var skillToBeAdded = db.Skills.First(x => x.Name == skill.Name);
            db.UsersSkills.Add(new UserSkill
            {
                SkillId = skillToBeAdded.Id,
                UserId = userId,
                Value = value,
            });
            db.SaveChanges();
        }

        public bool BelongsTo(int skillId, int userId)
        {
            return db.UsersSkills.Include(x => x.User).Any(x => x.Id == skillId && x.User.Id == userId);
        }

        public void DeleteForPosition(int positionId)
        {
            var positions = db.PositionSkills.Include(x => x.Position).Where(x => x.Position.Id == positionId).ToList();
            foreach (var item in positions)
            {
                db.PositionSkills.Remove(item);
            }
            db.SaveChanges();
        }

        public UserSkill Get(int id)
        {
            return db.UsersSkills.Find(id);
        }

        public List<Skill> GetAll()
        {
            return db.Skills.ToList();
        }

        public List<UserSkill> GetByUserId(int userId)
        {
            return db.UsersSkills
                .Include(x => x.Skill)
                .Include(x => x.User)
                .Where(x => x.User.Id == userId)
                .ToList();
        }

        public List<UserSkill> GetPagedByUserId(int userId, int page, int pageSize)
        {
            return db.UsersSkills
                .Where(x => x.User.Id == userId)
                .OrderByDescending(x => x.Value)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToList();
        }

        public List<Skill> MostUsedInPositions()
        {
            return db.Skills.Where(x => x.OfferSkills.Count > 0).OrderByDescending(x => x.OfferSkills.Count).Take(10).ToList();
        }

        public void RemoveFromUser(UserSkill skill)
        {
            db.UsersSkills.Remove(skill);
            db.SaveChanges();
        }
    }
}