﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RayProfile.Models
{
    public class User
    {
        private ICollection<Application> applications;

        private ICollection<CompanyVote> companyVotes;

        private ICollection<Education> education;

        private ICollection<Experience> experience;

        private ICollection<Message> messages;

        private ICollection<Resource> resources;

        private ICollection<UserSkill> userSkills;

        private ICollection<UserVote> userVotes;

        public User()
        {
            this.userSkills = new HashSet<UserSkill>();
            this.experience = new HashSet<Experience>();
            this.education = new HashSet<Education>();
            this.resources = new HashSet<Resource>();
            this.messages = new HashSet<Message>();
            this.companyVotes = new HashSet<CompanyVote>();
            this.applications = new HashSet<Application>();
            this.userVotes = new HashSet<UserVote>();
        }

        public virtual ICollection<Application> Applications
        {
            get { return this.applications; }
            set { this.applications = value; }
        }

        public virtual ICollection<CompanyVote> CompanyVotes
        {
            get { return this.companyVotes; }
            set { this.companyVotes = value; }
        }

        public UserDetails Details { get; set; }

        public virtual ICollection<Education> Education
        {
            get { return this.education; }
            set { this.education = value; }
        }

        [Required]
        [StringLength(150)]
        public string Email { get; set; }

        public virtual ICollection<Experience> Experience
        {
            get { return this.experience; }
            set { this.experience = value; }
        }

        [Key]
        public int Id { get; set; }

        public virtual ICollection<Message> Messages
        {
            get { return this.messages; }
            set { this.messages = value; }
        }

        [Required]
        [StringLength(200)]
        public string Password { get; set; }

        public virtual ICollection<Resource> Resources
        {
            get { return this.resources; }
            set { this.resources = value; }
        }

        [Required]
        [StringLength(150)]
        public string Username { get; set; }

        public virtual ICollection<UserSkill> UserSkills
        {
            get { return this.userSkills; }
            set { this.userSkills = value; }
        }

        public virtual ICollection<UserVote> UserVotes
        {
            get { return this.userVotes; }
            set { this.userVotes = value; }
        }
    }
}