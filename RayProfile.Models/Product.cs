﻿using System.ComponentModel.DataAnnotations;

namespace RayProfile.Models
{
    public class Product
    {
        public virtual Company Company { get; set; }

        [Required]
        public int CompanyId { get; set; }

        [Required]
        public string Desc { get; set; }

        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
    }
}