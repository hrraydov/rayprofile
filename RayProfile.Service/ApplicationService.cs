﻿using RayProfile.Data;
using RayProfile.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace RayProfile.Service
{
    public interface IApplicationService
    {
        /// <summary>
        /// Add new application
        /// </summary>
        /// <param name="application">Application to be added</param>
        void Add(Application application);

        /// <summary>
        /// Delete an application
        /// </summary>
        /// <param name="application">Application to be deleted</param>
        void Delete(Application application);

        /// <summary>
        /// Get application by id
        /// </summary>
        /// <param name="id">The id of an application</param>
        /// <returns></returns>
        Application GetById(int id);

        /// <summary>
        /// Get applications for a company
        /// </summary>
        /// <param name="id">The id of a company</param>
        /// <returns></returns>
        List<Application> GetForCompany(int id);

        /// <summary>
        /// Get applications for a position
        /// </summary>
        /// <param name="id">The id of a position</param>
        /// <returns></returns>
        List<Application> GetForPosition(int id);
    }

    public class ApplicationService : IApplicationService
    {
        private AppContext db = new AppContext();

        public void Add(Application application)
        {
            db.Applications.Add(application);
            db.SaveChanges();
        }

        public void Delete(Application application)
        {
            db.Applications.Remove(application);
            db.SaveChanges();
        }

        public Application GetById(int id)
        {
            return db.Applications.Include(x => x.Position).Include(x => x.User).First(x => x.Id == id);
        }

        public List<Application> GetForCompany(int id)
        {
            return db.Applications.Where(x => x.Position.Company.Id == id).ToList();
        }

        public List<Application> GetForPosition(int id)
        {
            return db.Applications.Where(x => x.Position.Id == id).ToList();
        }
    }
}