﻿using RayProfile.Data;
using RayProfile.Models;
using System.Linq;

namespace RayProfile.Service
{
    public interface IVoteService
    {
        /// <summary>
        /// Check if a company voted for a user
        /// </summary>
        /// <param name="companyId">The id of a company</param>
        /// <param name="userId">The id of a user</param>
        /// <returns></returns>
        bool CompanyVotedUser(int companyId, int userId);

        /// <summary>
        /// Get company vote
        /// </summary>
        /// <param name="userId">The id of a user</param>
        /// <param name="companyId">The id of a company</param>
        /// <returns></returns>
        CompanyVote GetCompanyVote(int userId, int companyId);

        /// <summary>
        /// Get user vote
        /// </summary>
        /// <param name="companyId">The id of a company</param>
        /// <param name="userId">The id of a user</param>
        /// <returns></returns>
        UserVote GetUserVote(int companyId, int userId);

        /// <summary>
        /// Check if user voted for company
        /// </summary>
        /// <param name="userId">The id of a user</param>
        /// <param name="companyId">The id of a company</param>
        /// <returns></returns>
        bool UserVotedCompany(int userId, int companyId);

        /// <summary>
        /// Vote for company
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="votedById"></param>
        /// <param name="value"></param>
        void VoteCompany(int companyId, int votedById, int value);

        /// <summary>
        /// Vote for user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="votedById"></param>
        /// <param name="value"></param>
        void VoteUser(int userId, int votedById, int value);
    }

    public class VoteService : IVoteService
    {
        private AppContext db = new AppContext();

        public bool CompanyVotedUser(int companyId, int userId)
        {
            if (!db.UserVotes.Any(x => x.VotedBy.Id == companyId && x.User.Id == userId))
            {
                return false;
            }
            return true;
        }

        public CompanyVote GetCompanyVote(int userId, int companyId)
        {
            return db.CompanyVotes.First(x => x.Company.Id == companyId && x.VotedBy.Id == userId);
        }

        public UserVote GetUserVote(int companyId, int userId)
        {
            return db.UserVotes.First(x => x.User.Id == userId && x.VotedBy.Id == companyId);
        }

        public bool UserVotedCompany(int userId, int companyId)
        {
            if (!db.CompanyVotes.Any(x => x.VotedBy.Id == userId && x.Company.Id == companyId))
            {
                return false;
            }
            return true;
        }

        public void VoteCompany(int companyId, int votedById, int value)
        {
            if (!this.UserVotedCompany(votedById, companyId))
            {
                db.CompanyVotes.Add(new CompanyVote
                {
                    CompanyId = companyId,
                    Value = value,
                    VotedById = votedById
                });
                db.SaveChanges();
            }
        }

        public void VoteUser(int userId, int votedById, int value)
        {
            if (!this.CompanyVotedUser(votedById, userId))
            {
                db.UserVotes.Add(new UserVote
                {
                    UserId = userId,
                    Value = value,
                    VotedById = votedById
                });
                db.SaveChanges();
            }
        }
    }
}