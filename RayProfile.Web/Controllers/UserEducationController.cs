﻿using RayProfile.Models;
using RayProfile.Service;
using RayProfile.Web.Filters;
using RayProfile.Web.ViewModels;
using System.Collections.Generic;
using System.Web.Mvc;

namespace RayProfile.Web.Controllers
{
    [IsUser]
    public class UserEducationController : BaseController
    {
        public UserEducationController()
            : base()
        {
        }

        public UserEducationController(IEducationService educationService, IUserService userService)
        {
            this.education = educationService;
            this.users = userService;
        }

        [HttpGet]
        [Route("user/education/create")]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("user/education/create")]
        public ActionResult Create([Bind(Include = "EndYear,SchoolName,StartYear,Type")]UserEducationCreateViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                this.education.Add(new Education
                {
                    EndYear = viewModel.EndYear,
                    SchoolName = viewModel.SchoolName,
                    StartYear = viewModel.StartYear,
                    UserId = this.users.GetByName(User.Identity.Name).Id,
                    Type = viewModel.Type,
                });
                return Redirect("/user/education");
            }
            return View(viewModel);
        }

        [HttpGet]
        [Route("user/education/delete/{id}")]
        public ActionResult Delete(int id)
        {
            if (this.belongsToCurrentUser(id))
            {
                Education education = this.education.GetById(id);
                this.education.Delete(education);
            }
            return Redirect("/user/education");
        }

        [HttpGet]
        [Route("user/education/edit/{id}")]
        public ActionResult Edit(int id)
        {
            if (this.belongsToCurrentUser(id))
            {
                var education = this.education.GetById(id);
                var viewModel = new UserEducationEditViewModel
                {
                    EndYear = education.EndYear,
                    Id = education.Id,
                    SchoolName = education.SchoolName,
                    StartYear = education.StartYear,
                    Type = education.Type,
                };
                return View(viewModel);
            }
            return Redirect("/user/education");
        }

        [Route("user/education/edit/{id}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,EndYear,SchoolName,StartYear,Type")]UserEducationEditViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                if (this.belongsToCurrentUser(viewModel.Id))
                {
                    var education = this.education.GetById(viewModel.Id);
                    education.SchoolName = viewModel.SchoolName;
                    education.StartYear = viewModel.StartYear;
                    education.EndYear = viewModel.EndYear;
                    education.Type = viewModel.Type;
                    this.education.Edit(education);
                }
                return Redirect("/user/education");
            }
            return View(viewModel);
        }

        [HttpGet]
        [Route("user/education")]
        public ActionResult Index()
        {
            var education = this.education.GetByUserNameOrderByDesc(User.Identity.Name);
            var viewModel = new List<UserEducationIndexViewModel>();
            foreach (var item in education)
            {
                viewModel.Add(new UserEducationIndexViewModel
                {
                    EndYear = item.EndYear,
                    Id = item.Id,
                    SchoolName = item.SchoolName,
                    StartYear = item.StartYear,
                    Type = this.education.GetEducationType(item.Type),
                });
            }
            return View(viewModel);
        }

        private bool belongsToCurrentUser(int id)
        {
            return this.education.BelongsTo(id, CurrentUser.Id);
        }
    }
}