﻿using RayProfile.Models;
using System.ComponentModel.DataAnnotations;

namespace RayProfile.Web.ViewModels
{
    public class UserEducationCreateViewModel
    {
        [Display(Name = "EndYear",
            ResourceType = typeof(Resources.ViewModels.UserEducation))]
        public short? EndYear { get; set; }

        [Display(Name = "SchoolName",
            ResourceType = typeof(Resources.ViewModels.UserEducation))]
        [Required(
            ErrorMessageResourceName = "SchoolNameRequired",
            ErrorMessageResourceType = typeof(Resources.ViewModels.UserEducation)
                )]
        [StringLength(300,
            ErrorMessageResourceName = "SchoolNameLength",
            ErrorMessageResourceType = typeof(Resources.ViewModels.UserEducation))
        ]
        public string SchoolName { get; set; }

        [Required(
            ErrorMessageResourceName = "StartYearRequired",
            ErrorMessageResourceType = typeof(Resources.ViewModels.UserEducation)
                )]
        [Display(Name = "StartYear",
            ResourceType = typeof(Resources.ViewModels.UserEducation))]
        public short StartYear { get; set; }

        [Display(Name = "Type",
            ResourceType = typeof(Resources.ViewModels.UserEducation))]
        public EducationType Type { get; set; }
    }

    public class UserEducationEditViewModel
    {
        [Display(Name = "EndYear",
            ResourceType = typeof(Resources.ViewModels.UserEducation))]
        public short? EndYear { get; set; }

        public int Id { get; set; }

        [Display(Name = "SchoolName",
            ResourceType = typeof(Resources.ViewModels.UserEducation))]
        [Required(
            ErrorMessageResourceName = "SchoolNameRequired",
            ErrorMessageResourceType = typeof(Resources.ViewModels.UserEducation)
                )]
        [StringLength(300,
            ErrorMessageResourceName = "SchoolNameLength",
            ErrorMessageResourceType = typeof(Resources.ViewModels.UserEducation)
            )]
        public string SchoolName { get; set; }

        [Required(
            ErrorMessageResourceName = "StartYearRequired",
            ErrorMessageResourceType = typeof(Resources.ViewModels.UserEducation)
                )]
        [Display(Name = "StartYear",
            ResourceType = typeof(Resources.ViewModels.UserEducation))]
        public short StartYear { get; set; }

        [Display(Name = "Type",
            ResourceType = typeof(Resources.ViewModels.UserEducation))]
        public EducationType Type { get; set; }
    }

    public class UserEducationIndexViewModel
    {
        public short? EndYear { get; set; }

        public int Id { get; set; }

        public string SchoolName { get; set; }

        public short StartYear { get; set; }

        public string Type { get; set; }
    }
}