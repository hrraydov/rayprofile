﻿namespace RayProfile.Models
{
    public enum CompanySize
    {
        OneToTen = 1,
        TenToFifty = 2,
        FiftyToHundred = 3,
        HundredToFiveH = 4,
        FiveHToThousand = 5,
        ThousandToFiveT = 6,
        FiveTToTenT = 7,
        OverTenT = 8
    }
}