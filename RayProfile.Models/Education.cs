﻿using System.ComponentModel.DataAnnotations;

namespace RayProfile.Models
{
    public class Education
    {
        public short? EndYear { get; set; }

        [Key]
        public int Id { get; set; }

        [Required]
        public string SchoolName { get; set; }

        [Required]
        public short StartYear { get; set; }

        [Required]
        public EducationType Type { get; set; }

        public virtual User User { get; set; }

        public int UserId { get; set; }
    }
}