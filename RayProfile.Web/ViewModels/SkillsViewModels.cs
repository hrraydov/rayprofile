﻿namespace RayProfile.Web.ViewModels
{
    public class SkillsShowMostRequiredViewModel
    {
        public int Count { get; set; }

        public string Name { get; set; }
    }
}