﻿namespace RayProfile.Models
{
    public enum EducationType
    {
        Primary = 1,
        Elementary = 2,
        Secondary = 3,
        University = 4
    }
}