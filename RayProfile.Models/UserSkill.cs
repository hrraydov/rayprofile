﻿using System.ComponentModel.DataAnnotations;

namespace RayProfile.Models
{
    public class UserSkill
    {
        [Key]
        public int Id { get; set; }

        public virtual Skill Skill { get; set; }

        [Required]
        public int SkillId { get; set; }

        public virtual User User { get; set; }

        [Required]
        public int UserId { get; set; }

        [Required]
        public int Value { get; set; }
    }
}