﻿using System.ComponentModel.DataAnnotations;

namespace RayProfile.Web.ViewModels
{
    public class ProductsAddViewModel
    {
        [Required]
        [Display(
            Name = "Desc",
            ResourceType = typeof(Resources.ViewModels.Products)
            )]
        public string Desc { get; set; }

        [Required]
        [Display(
            Name = "Name",
            ResourceType = typeof(Resources.ViewModels.Products)
            )]
        public string Name { get; set; }
    }

    public class ProductsEditViewModel
    {
        [Required]
        [Display(
            Name = "Desc",
            ResourceType = typeof(Resources.ViewModels.Products)
            )]
        public string Desc { get; set; }

        public int Id { get; set; }

        [Required]
        [Display(
            Name = "Name",
            ResourceType = typeof(Resources.ViewModels.Products)
            )]
        public string Name { get; set; }
    }

    public class ProductsIndexViewModel
    {
        public string Desc { get; set; }

        public int Id { get; set; }

        public string Name { get; set; }
    }
}