﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18449
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RayProfile.Resources.ViewModels {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class UserEducation {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal UserEducation() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("RayProfile.Resources.ViewModels.UserEducation", typeof(UserEducation).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to End year.
        /// </summary>
        public static string EndYear {
            get {
                return ResourceManager.GetString("EndYear", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to School name.
        /// </summary>
        public static string SchoolName {
            get {
                return ResourceManager.GetString("SchoolName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to School name length myst be maximum {0} symbols.
        /// </summary>
        public static string SchoolNameLength {
            get {
                return ResourceManager.GetString("SchoolNameLength", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The school name is required.
        /// </summary>
        public static string SchoolNameRequired {
            get {
                return ResourceManager.GetString("SchoolNameRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Start year.
        /// </summary>
        public static string StartYear {
            get {
                return ResourceManager.GetString("StartYear", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The start year is required.
        /// </summary>
        public static string StartYearRequired {
            get {
                return ResourceManager.GetString("StartYearRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Type.
        /// </summary>
        public static string Type {
            get {
                return ResourceManager.GetString("Type", resourceCulture);
            }
        }
    }
}
