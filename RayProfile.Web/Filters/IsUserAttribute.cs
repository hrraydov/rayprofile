﻿using System.Web.Mvc;

namespace RayProfile.Web.Filters
{
    public class IsUserAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string type = filterContext.HttpContext.Session["type"].ToString();
            if (filterContext.HttpContext.User.Identity.IsAuthenticated != true || type != "user")
            {
                filterContext.Result = new RedirectResult("/");
            }
            base.OnActionExecuting(filterContext);
        }
    }
}