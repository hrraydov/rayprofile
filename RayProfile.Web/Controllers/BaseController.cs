﻿using RayProfile.Models;
using RayProfile.Service;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace RayProfile.Web.Controllers
{
    public abstract class BaseController : Controller
    {
        protected IApplicationService applications;
        protected ICompanyService companies;
        protected ICompanyDetailsService companyDetails;
        protected IEducationService education;
        protected IExperienceService experience;
        protected IMessageService messages;
        protected IPositionService positions;
        protected IProductService products;
        protected IResourceService resources;
        protected ISkillService skills;
        protected IUserService users;
        protected IVoteService votes;

        private HashSet<string> allowedLanguages = new HashSet<string> {
            "en",
            "bg"
        };

        private Company currentCompany = null;
        private User currentUser = null;
        private string defaultLanguage = "en";

        public BaseController()
        {
            this.users = new UserService();
            this.skills = new SkillService();
            this.companies = new CompanyService();
            this.experience = new ExperienceService();
            this.education = new EducationService();
            this.resources = new ResourceService();
            this.products = new ProductService();
            this.companyDetails = new CompanyDetailsService();
            this.positions = new PositionService();
            this.applications = new ApplicationService();
            this.messages = new MessageService();
            this.votes = new VoteService();
        }

        protected Company CurrentCompany
        {
            get
            {
                if (Session["type"] != null && Session["type"].ToString() == "company")
                {
                    if (this.currentCompany == null)
                    {
                        this.currentCompany = this.companies.GetByLoginName(User.Identity.Name);
                    }
                    return this.currentCompany;
                }
                else
                {
                    return null;
                }
            }
        }

        protected User CurrentUser
        {
            get
            {
                if (Session["type"] != null && Session["type"].ToString() == "user")
                {
                    if (this.currentUser == null)
                    {
                        this.currentUser = this.users.GetByName(User.Identity.Name);
                    }
                    return this.currentUser;
                }
                else
                {
                    return null;
                }
            }
        }

        protected override IAsyncResult BeginExecute(System.Web.Routing.RequestContext requestContext, AsyncCallback callback, object state)
        {
            var cultureName = "";

            if (requestContext.HttpContext.Request.Cookies["_lang"] == null)
            {
                HttpCookie cookie = new HttpCookie("_lang");
                if (cookie.Value == null)
                {
                    cookie.Expires = DateTime.Now.AddDays(1);
                    cookie.Value = "en";
                }
                requestContext.HttpContext.Request.Cookies.Add(cookie);
            }
            cultureName = requestContext.HttpContext.Request.Cookies["_lang"].Value.ToString();

            if (!allowedLanguages.Contains(cultureName))
            {
                cultureName = defaultLanguage;
            }

            Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(cultureName);
            return base.BeginExecute(requestContext, callback, state);
        }

        protected override void EndExecute(IAsyncResult asyncResult)
        {
            Response.Cookies.Add(Request.Cookies["_lang"]);
            base.EndExecute(asyncResult);
        }
    }
}