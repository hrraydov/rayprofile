﻿using RayProfile.Models;
using RayProfile.Service;
using RayProfile.Web.Filters;
using RayProfile.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace RayProfile.Web.Controllers
{
    [IsCompany]
    public class PositionsController : BaseController
    {
        public PositionsController()
            : base()
        {
        }

        public PositionsController(IPositionService positionService, ISkillService skillService)
        {
            this.positions = positionService;
            this.skills = skillService;
        }

        [Route("company/position/add")]
        [HttpGet]
        public ActionResult Add()
        {
            var viewModel = new PositionsAddViewModel();
            var skills = this.skills.GetAll();
            var skillNames = skills.Select(x => x.Name).ToList();
            var jsonSerializer = new JavaScriptSerializer();
            viewModel.SkillsJSON = jsonSerializer.Serialize(skillNames);
            return View(viewModel);
        }

        [Route("company/position/add")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Add(PositionsAddViewModel viewModel)
        {
            var skills = new List<PositionSkillModel>();
            if (viewModel.Skills != null)
            {
                skills = new List<PositionSkillModel>(viewModel.Skills);

                foreach (var item in viewModel.Skills)
                {
                    if (item.Name == null || item.Value == null)
                    {
                        item.Name = "ex";
                        skills.Remove(item);
                    }
                }
            }
            if (ModelState.IsValid)
            {
                var position = this.positions.Add(new Position
                {
                    CompanyId = CurrentCompany.Id,
                    Desc = viewModel.Desc,
                    Name = viewModel.Name,
                });
                foreach (var skill in skills)
                {
                    this.skills.AddToPosition(new Skill
                    {
                        Name = skill.Name
                    }, position.Id, Convert.ToInt32(skill.Value));
                }
                return Redirect("/company/positions");
            }
            return View(viewModel);
        }

        [Route("company/position/delete/{id}")]
        [HttpGet]
        public ActionResult Delete(int id)
        {
            if (this.belongsToCurrentCompany(id))
            {
                this.positions.Delete(id);
            }
            return Redirect("/company/positions");
        }

        [Route("company/position/edit/{id}")]
        [HttpGet]
        public ActionResult Edit(int id)
        {
            if (this.belongsToCurrentCompany(id))
            {
                var position = this.positions.Get(id);
                var viewModel = new PositionsEditViewModel
                {
                    Desc = position.Desc,
                    Id = position.Id,
                    Name = position.Name,
                };
                viewModel.Skills = new List<PositionSkillModel>();
                foreach (var item in position.Skills)
                {
                    viewModel.Skills.Add(new PositionSkillModel
                    {
                        Name = item.Skill.Name,
                        Value = item.Value,
                    });
                }
                var serializer = new JavaScriptSerializer();
                viewModel.SkillsJSON = serializer.Serialize(this.skills.GetAll().Select(x => x.Name).ToList());
                viewModel.ValuesJSON = serializer.Serialize(viewModel.Skills.Select(x => x.Value).ToList());
                return View(viewModel);
            }
            return Redirect("/company/positions");
        }

        [Route("company/position/edit/{id}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(PositionsEditViewModel viewModel)
        {
            var skills = new List<PositionSkillModel>();
            if (viewModel.Skills != null)
            {
                skills = new List<PositionSkillModel>(viewModel.Skills);

                foreach (var item in viewModel.Skills)
                {
                    if (item.Name == null || item.Value == null)
                    {
                        item.Name = "ex";
                        skills.Remove(item);
                    }
                }
            }
            if (ModelState.IsValid)
            {
                if (this.belongsToCurrentCompany(viewModel.Id))
                {
                    this.skills.DeleteForPosition(viewModel.Id);
                    this.positions.Delete(viewModel.Id);
                    var position = this.positions.Add(new Position
                    {
                        CompanyId = CurrentCompany.Id,
                        Desc = viewModel.Desc,
                        Name = viewModel.Name,
                    });
                    foreach (var skill in skills)
                    {
                        this.skills.AddToPosition(new Skill
                        {
                            Name = skill.Name
                        }, position.Id, Convert.ToInt32(skill.Value));
                    }
                }
                return Redirect("/company/positions");
            }
            return View(viewModel);
        }

        [Route("company/positions")]
        [HttpGet]
        public ActionResult Index()
        {
            var positions = this.positions.GetForCompany(CurrentCompany.Id);
            var viewModel = new List<PositionsIndexViewModel>();
            foreach (var item in positions)
            {
                var skills = new List<PositionSkillModel>();
                foreach (var skill in item.Skills)
                {
                    skills.Add(new PositionSkillModel
                    {
                        Name = skill.Skill.Name,
                        Value = skill.Value,
                    });
                }
                viewModel.Add(new PositionsIndexViewModel
                {
                    Desc = item.Desc,
                    Id = item.Id,
                    Name = item.Name,
                    Skills = skills,
                });
            }
            return View(viewModel);
        }

        private bool belongsToCurrentCompany(int id)
        {
            return this.positions.BelongsTo(id, CurrentCompany.Id);
        }
    }
}