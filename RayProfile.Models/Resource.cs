﻿using System.ComponentModel.DataAnnotations;

namespace RayProfile.Models
{
    public class Resource
    {
        public string Content { get; set; }

        [Key]
        public int Id { get; set; }

        [Required]
        public int Importance { get; set; }

        public string Link { get; set; }

        [Required]
        public string Title { get; set; }

        public virtual User User { get; set; }

        [Required]
        public int UserId { get; set; }
    }
}