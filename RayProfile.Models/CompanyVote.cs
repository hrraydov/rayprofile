﻿using System.ComponentModel.DataAnnotations;

namespace RayProfile.Models
{
    public class CompanyVote
    {
        public virtual Company Company { get; set; }

        [Required]
        public int CompanyId { get; set; }

        [Key]
        public int Id { get; set; }

        [Required]
        public int Value { get; set; }

        public virtual User VotedBy { get; set; }

        [Required]
        public int VotedById { get; set; }
    }
}