﻿using System.ComponentModel.DataAnnotations;

namespace RayProfile.Models
{
    public class PositionSkill
    {
        [Key]
        public int Id { get; set; }

        public virtual Position Position { get; set; }

        [Required]
        public int PositionId { get; set; }

        public virtual Skill Skill { get; set; }

        [Required]
        public int SkillId { get; set; }

        [Required]
        public int Value { get; set; }
    }
}