﻿using System.ComponentModel.DataAnnotations;

namespace RayProfile.Web.ViewModels
{
    public class VotesVoteCompanyViewViewModel
    {
        public int CompanyId { get; set; }

        [Required]
        [Range(1, 5)]
        public int Value { get; set; }

        public bool Voted { get; set; }
    }

    public class VotesVoteUserViewViewModel
    {
        public int UserId { get; set; }

        [Required]
        [Range(1, 5)]
        public int Value { get; set; }

        public bool Voted { get; set; }
    }
}