﻿using System.Collections.Generic;

namespace RayProfile.Web.ViewModels
{
    public class OffersAddViewModel
    {
        public string Desc { get; set; }

        public string Position { get; set; }

        public string Skills { get; set; }
    }

    public class OffersIndexViewModel
    {
        public string Desc { get; set; }

        public int Id { get; set; }

        public string Position { get; set; }

        public List<OfferSkill> Skills { get; set; }
    }

    public class OfferSkill
    {
        public string Name { get; set; }

        public int Value { get; set; }
    }
}