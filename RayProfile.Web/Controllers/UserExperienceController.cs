﻿using RayProfile.Models;
using RayProfile.Service;
using RayProfile.Web.Filters;
using RayProfile.Web.ViewModels;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace RayProfile.Web.Controllers
{
    [IsUser]
    public class UserExperienceController : BaseController
    {
        public UserExperienceController()
            : base()
        {
        }

        public UserExperienceController(ICompanyService companyService, IExperienceService experienceService)
        {
            this.companies = companyService;
            this.experience = experienceService;
        }

        [Route("user/experience/add")]
        [HttpGet]
        public ActionResult Add()
        {
            var viewModel = new UserExperienceAddViewModel();
            var companyNames = this.companies.GetAllNames();
            var serializer = new JavaScriptSerializer();
            viewModel.CompanyNamesJSON = serializer.Serialize(companyNames);
            return View(viewModel);
        }

        [Route("user/experience/add")]
        [HttpPost]
        public ActionResult Add(UserExperienceAddViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                this.experience.Add(new Experience
                {
                    CompanyName = viewModel.CompanyName,
                    EndYear = viewModel.EndYear,
                    Position = viewModel.Position,
                    StartYear = viewModel.StartYear,
                    UserId = CurrentUser.Id,
                });
                return Redirect("/user/experience");
            }
            var companyNames = this.companies.GetAllNames();
            var serializer = new JavaScriptSerializer();
            viewModel.CompanyNamesJSON = serializer.Serialize(companyNames);
            return View(viewModel);
        }

        [Route("user/experience/delete/{id}")]
        [HttpGet]
        public ActionResult Delete(int id)
        {
            if (this.belongsToCurrentUser(id))
            {
                this.experience.Delete(id);
            }
            return Redirect("/user/experience");
        }

        [HttpGet]
        [Route("user/experience/edit/{id}")]
        public ActionResult Edit(int id)
        {
            if (this.belongsToCurrentUser(id))
            {
                var experience = this.experience.Get(id);
                var companies = this.companies.GetAllNames();
                var serializer = new JavaScriptSerializer();
                var viewModel = new UserExperienceEditViewModel
                {
                    CompanyName = experience.CompanyName,
                    CompanyNamesJSON = serializer.Serialize(companies),
                    EndYear = experience.EndYear,
                    Id = experience.Id,
                    Position = experience.Position,
                    StartYear = experience.StartYear,
                };
                return View(viewModel);
            }
            return Redirect("/user/experience");
        }

        [Route("user/experience/edit/{id}")]
        [HttpPost]
        public ActionResult Edit(UserExperienceEditViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                if (this.belongsToCurrentUser(viewModel.Id))
                {
                    var experience = new Experience
                    {
                        CompanyName = viewModel.CompanyName,
                        EndYear = viewModel.EndYear,
                        Id = viewModel.Id,
                        Position = viewModel.Position,
                        StartYear = viewModel.StartYear,
                        UserId = CurrentUser.Id,
                    };
                    this.experience.Edit(experience);
                }
                return Redirect("/user/experience");
            }
            var companies = this.companies.GetAllNames();
            var serializer = new JavaScriptSerializer();
            viewModel.CompanyNamesJSON = serializer.Serialize(companies);
            return View(viewModel);
        }

        [Route("user/experience")]
        [HttpGet]
        public ActionResult Index()
        {
            var experience = this.experience.GetForUser(CurrentUser.Id);
            var viewModel = new List<UserExperienceIndexViewModel>();
            foreach (var item in experience)
            {
                viewModel.Add(new UserExperienceIndexViewModel
                {
                    Id = item.Id,
                    CompanyName = item.CompanyName,
                    CompanyExists = this.companies.ExistsByName(item.CompanyName),
                    EndYear = item.EndYear,
                    StartYear = item.StartYear,
                    Position = item.Position,
                });
            }

            foreach (var item in viewModel)
            {
                if (item.CompanyExists)
                {
                    item.CompanyId = this.companies.GetByName(item.CompanyName).Id;
                }
            }
            return View(viewModel);
        }

        private bool belongsToCurrentUser(int id)
        {
            return this.experience.BelongsTo(id, CurrentUser.Id);
        }
    }
}