﻿using RayProfile.Data;
using RayProfile.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace RayProfile.Service
{
    public interface IProductService
    {
        /// <summary>
        /// Add a product
        /// </summary>
        /// <param name="product">The product to be added</param>
        void Add(Product product);

        /// <summary>
        /// Chack if a product belongs to a company
        /// </summary>
        /// <param name="productId">The id of a product</param>
        /// <param name="companyId">The id of a company</param>
        /// <returns></returns>
        bool BelongsTo(int productId, int companyId);

        /// <summary>
        /// Delete a product
        /// </summary>
        /// <param name="id">The id of the product to be deleted</param>
        void Delete(int id);

        /// <summary>
        /// Edit a product
        /// </summary>
        /// <param name="product">The product to be edited</param>
        void Edit(Product product);

        /// <summary>
        /// Get a product by id
        /// </summary>
        /// <param name="id">The id of a product</param>
        /// <returns></returns>
        Product GetById(int id);

        /// <summary>
        /// Get products for company
        /// </summary>
        /// <param name="companyId">The id of a company</param>
        /// <returns></returns>
        List<Product> GetForCompany(int companyId);
    }

    public class ProductService : IProductService
    {
        private AppContext db = new AppContext();

        public void Add(Product product)
        {
            db.Products.Add(product);
            db.SaveChanges();
        }

        public bool BelongsTo(int productId, int companyId)
        {
            return db.Products.Include(x => x.Company).Any(x => x.Id == productId && x.Company.Id == companyId);
        }

        public void Delete(int id)
        {
            var product = db.Products.Find(id);
            db.Products.Remove(product);
            db.SaveChanges();
        }

        public void Edit(Product product)
        {
            db.Entry(product).State = EntityState.Modified;
            db.SaveChanges();
        }

        public Product GetById(int id)
        {
            return db.Products.Find(id);
        }

        public List<Product> GetForCompany(int companyId)
        {
            return db.Products.Where(x => x.Company.Id == companyId).OrderBy(x => x.Name).ToList();
        }
    }
}