﻿using RayProfile.Models;
using RayProfile.Service;
using RayProfile.Web.Filters;
using RayProfile.Web.ViewModels;
using System.Collections.Generic;
using System.Web.Mvc;

namespace RayProfile.Web.Controllers
{
    [IsCompany]
    public class ProductsController : BaseController
    {
        public ProductsController()
            : base()
        {
        }

        public ProductsController(IProductService productService)
        {
            this.products = productService;
        }

        [Route("company/product/add")]
        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }

        [Route("company/product/add")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(ProductsAddViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                this.products.Add(new Product
                {
                    Name = viewModel.Name,
                    Desc = viewModel.Desc,
                    CompanyId = CurrentCompany.Id,
                });
                return Redirect("/company/products");
            }
            return View(viewModel);
        }

        [Route("company/product/delete/{id}")]
        [HttpGet]
        public ActionResult Delete(int id)
        {
            if (this.belongsToCurrentCompany(id))
            {
                this.products.Delete(id);
            }
            return Redirect("/company/products");
        }

        [Route("company/product/edit/{id}")]
        [HttpGet]
        public ActionResult Edit(int id)
        {
            if (this.belongsToCurrentCompany(id))
            {
                var product = this.products.GetById(id);
                var viewModel = new ProductsEditViewModel
                {
                    Desc = product.Desc,
                    Id = product.Id,
                    Name = product.Name,
                };
                return View(viewModel);
            }
            return Redirect("/company/products");
        }

        [Route("company/product/edit/{id}")]
        [HttpPost]
        public ActionResult Edit(ProductsEditViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                if (this.belongsToCurrentCompany(viewModel.Id))
                {
                    this.products.Edit(new Product
                    {
                        CompanyId = CurrentCompany.Id,
                        Desc = viewModel.Desc,
                        Id = viewModel.Id,
                        Name = viewModel.Name,
                    });
                }
                return Redirect("/company/products");
            }
            return View(viewModel);
        }

        [Route("company/products")]
        [HttpGet]
        public ActionResult Index()
        {
            var products = this.products.GetForCompany(CurrentCompany.Id);
            var viewModel = new List<ProductsIndexViewModel>();
            foreach (var item in products)
            {
                viewModel.Add(new ProductsIndexViewModel
                {
                    Id = item.Id,
                    Name = item.Name,
                    Desc = item.Desc,
                });
            }
            return View(viewModel);
        }

        private bool belongsToCurrentCompany(int id)
        {
            return this.products.BelongsTo(id, CurrentCompany.Id);
        }
    }
}