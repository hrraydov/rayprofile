﻿using System.ComponentModel.DataAnnotations;

namespace RayProfile.Models
{
    public class Application
    {
        [Key]
        public int Id { get; set; }

        public virtual Position Position { get; set; }

        [Required]
        public int PositionId { get; set; }

        public virtual User User { get; set; }

        [Required]
        public int UserId { get; set; }
    }
}