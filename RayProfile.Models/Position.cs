﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RayProfile.Models
{
    public class Position
    {
        private ICollection<Application> applications;

        private ICollection<PositionSkill> skills;

        public Position()
        {
            this.skills = new HashSet<PositionSkill>();
            this.applications = new HashSet<Application>();
        }

        public virtual ICollection<Application> Applications
        {
            get { return this.applications; }
            set { this.applications = value; }
        }

        public virtual Company Company { get; set; }

        [Required]
        public int CompanyId { get; set; }

        [Required]
        [StringLength(4000)]
        public string Desc { get; set; }

        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(150)]
        public string Name { get; set; }

        public virtual ICollection<PositionSkill> Skills
        {
            get { return this.skills; }
            set { this.skills = value; }
        }
    }
}