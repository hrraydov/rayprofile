﻿using RayProfile.Service;
using RayProfile.Web.Filters;
using RayProfile.Web.ViewModels;
using System.Web.Mvc;

namespace RayProfile.Web.Controllers
{
    [IsCompany]
    public class CompanyDetailsController : BaseController
    {
        public CompanyDetailsController()
            : base()
        {
        }

        public CompanyDetailsController(ICompanyService companyService)
        {
            this.companies = companyService;
        }

        [Route("company/details")]
        [HttpGet]
        public ActionResult Edit()
        {
            var company = this.companies.GetById(CurrentCompany.Id);
            var viewModel = new CompanyDetailsEditViewModel
            {
                Desc = company.Details.Desc,
                Founded = company.Details.Founded,
                Industry = company.Details.Industry,
                Site = company.Details.Site,
                Size = company.Details.Size,
                SizeStr = this.companyDetails.CompanySizeToString(company.Details.Size)
            };
            return View(viewModel);
        }

        [Route("company/details")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(CompanyDetailsEditViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var company = CurrentCompany;
                company.Details.Desc = viewModel.Desc;
                company.Details.Founded = viewModel.Founded;
                company.Details.Industry = viewModel.Industry;
                company.Details.Site = viewModel.Site;
                company.Details.Size = viewModel.Size;
                this.companies.Edit(company);
                return Redirect("/company/details");
            }
            return View(viewModel);
        }
    }
}