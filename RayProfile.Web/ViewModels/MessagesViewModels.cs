﻿namespace RayProfile.Web.ViewModels
{
    public class MessagesReceivedViewModel
    {
        public string Content { get; set; }
    }
}