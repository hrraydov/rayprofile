﻿using RayProfile.Data;
using RayProfile.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace RayProfile.Service
{
    public interface IPositionService
    {
        /// <summary>
        /// Add a position
        /// </summary>
        /// <param name="position">The position to be added</param>
        /// <returns></returns>
        Position Add(Position position);

        /// <summary>
        /// Check if a position belongs to a company
        /// </summary>
        /// <param name="positionId">The id of a position</param>
        /// <param name="companyId">The id of a company</param>
        /// <returns></returns>
        bool BelongsTo(int positionId, int companyId);

        /// <summary>
        /// Delete a position
        /// </summary>
        /// <param name="id">The id of the position to be deleted</param>
        void Delete(int id);

        /// <summary>
        /// Get a position by id
        /// </summary>
        /// <param name="id">The id of a position</param>
        /// <returns></returns>
        Position Get(int id);

        /// <summary>
        /// Get positions for company
        /// </summary>
        /// <param name="id">The id of a company</param>
        /// <returns></returns>
        List<Position> GetForCompany(int id);
    }

    public class PositionService : IPositionService
    {
        private AppContext db = new AppContext();

        public Position Add(Position position)
        {
            var positionAdded = db.Positions.Add(position);
            db.SaveChanges();
            return positionAdded;
        }

        public bool BelongsTo(int positionId, int companyId)
        {
            return db.Positions.Include(x => x.Company).Any(x => x.Id == positionId && x.Company.Id == companyId);
        }

        public void Delete(int id)
        {
            var position = db.Positions.Find(id);
            db.Positions.Remove(position);
            db.SaveChanges();
        }

        public Position Get(int id)
        {
            return db.Positions.Find(id);
        }

        public List<Position> GetForCompany(int id)
        {
            return db.Positions.Where(x => x.Company.Id == id).ToList();
        }
    }
}