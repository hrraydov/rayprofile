﻿using RayProfile.Models;
using RayProfile.Service;
using RayProfile.Web.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Security;

namespace RayProfile.Web.Controllers
{
    public class CompanyController : BaseController
    {
        public CompanyController()
            : base()
        {
        }

        public CompanyController(ICompanyService companyService, IPositionService positionService, IProductService productService)
        {
            this.companies = companyService;
            this.positions = positionService;
            this.products = productService;
        }

        [Route("company/best")]
        public ActionResult Best()
        {
            var companies = this.companies.GetBest();
            var viewModel = new List<CompanyBestViewModel>();
            foreach (var item in companies)
            {
                var values = item.CompanyVotes.Select(x => x.Value).ToList();
                viewModel.Add(new CompanyBestViewModel
                {
                    AverageVote = values.Average(),
                    Id = item.Id,
                    Name = item.Name,
                    VotesCount = values.Count
                });
            }
            return PartialView(viewModel);
        }

        [Route("login/company")]
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [Route("login/company")]
        [HttpPost]
        public ActionResult Login(CompanyLoginViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                viewModel.LoginName = viewModel.LoginName.Trim();
                viewModel.Password = viewModel.Password.Trim();
                if (
                    !this.companies.Exists(viewModel.LoginName) ||
                    !Crypto.VerifyHashedPassword(
                    this.companies.GetByLoginName(viewModel.LoginName).Password,
                    viewModel.Password)
                    )
                {
                    ModelState.AddModelError("", Resources.Controllers.Company.CompanyNotExists);
                }
                if (ModelState.IsValid)
                {
                    FormsAuthentication.SetAuthCookie(viewModel.LoginName, false);
                    Session["type"] = "company";
                    return Redirect("/");
                }
                return View(viewModel);
            }
            return View(viewModel);
        }

        [Route("positions")]
        public ActionResult Positions(int companyId)
        {
            var positions = this.positions.GetForCompany(companyId);
            var viewModel = new List<CompanyPositionsViewModel>();
            foreach (var item in positions)
            {
                var skills = new List<PositionSkillModel>();
                Models.User user = CurrentUser;

                foreach (var skill in item.Skills)
                {
                    skills.Add(new PositionSkillModel
                    {
                        Name = skill.Skill.Name,
                        Value = skill.Value,
                    });
                }
                viewModel.Add(new CompanyPositionsViewModel
                {
                    Id = item.Id,
                    CurrentUser = user,
                    Desc = item.Desc,
                    Name = item.Name,
                    Skills = skills,
                });
            }
            return PartialView(viewModel);
        }

        [Route("products")]
        public ActionResult Products(int companyId)
        {
            var products = this.products.GetForCompany(companyId);
            var viewModel = new List<CompanyProductsViewModel>();
            foreach (var item in products)
            {
                viewModel.Add(new CompanyProductsViewModel
                {
                    Name = item.Name,
                    Desc = item.Desc,
                });
            }
            return PartialView(viewModel);
        }

        [Route("register/company")]
        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }

        [Route("register/company")]
        [HttpPost]
        public ActionResult Register(CompanyRegisterViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                viewModel.Email = viewModel.Email.Trim();
                viewModel.LoginName = viewModel.LoginName.Trim();
                viewModel.Name = viewModel.Name.Trim();
                viewModel.Password = viewModel.Password.Trim();
                if (this.companies.CompanyLoginOrNameOrEmailExists(viewModel.LoginName, viewModel.Name, viewModel.Email))
                {
                    ModelState.AddModelError("", Resources.Controllers.Company.CompanyExists);
                }
                if (ModelState.IsValid)
                {
                    this.companies.Register(new Company
                    {
                        Email = viewModel.Email,
                        LoginName = viewModel.LoginName,
                        Name = viewModel.Name,
                        Password = Crypto.HashPassword(viewModel.Password),
                        Details = new CompanyDetails(),
                    });
                    FormsAuthentication.SetAuthCookie(viewModel.LoginName, false);
                    Session["type"] = "company";
                    return Redirect("/");
                }
                return View(viewModel);
            }
            return View(viewModel);
        }

        [Route("company/{id}")]
        [HttpGet]
        public ActionResult Show(int id)
        {
            var company = this.companies.GetById(id);
            var viewModel = new CompanyShowViewModel
            {
                Id = company.Id,
                Name = company.Name,
                Desc = company.Details.Desc,
                Founded = company.Details.Founded,
                Industry = company.Details.Industry,
                Site = company.Details.Site,
                Size = this.companyDetails.CompanySizeToString(company.Details.Size),
            };
            return View(viewModel);
        }
    }
}