﻿using RayProfile.Service;
using RayProfile.Web.Filters;
using RayProfile.Web.ViewModels;
using System.Web.Mvc;

namespace RayProfile.Web.Controllers
{
    public class VotesController : BaseController
    {
        public VotesController()
            : base()
        {
        }

        public VotesController(IVoteService voteService)
        {
            this.votes = voteService;
        }

        [Route("company/vote/{companyId}")]
        [HttpPost]
        [IsUser]
        public ActionResult VoteCompany(int companyId, int value)
        {
            this.votes.VoteCompany(companyId, CurrentUser.Id, value);
            return Redirect("/");
        }

        [Route("company/vote")]
        [IsUser]
        public ActionResult VoteCompanyView(int companyId)
        {
            var viewModel = new VotesVoteCompanyViewViewModel();
            viewModel.CompanyId = companyId;
            if (CurrentUser != null)
                if (this.votes.UserVotedCompany(CurrentUser.Id, companyId))
                {
                    viewModel.Voted = true;
                    viewModel.Value = this.votes.GetCompanyVote(CurrentUser.Id, companyId).Value;
                }
                else
                {
                    viewModel.Voted = false;
                }
            return PartialView(viewModel);
        }

        [Route("user/vote/{userId}")]
        [HttpPost]
        [IsCompany]
        public ActionResult VoteUser(int userId, int value)
        {
            this.votes.VoteUser(userId, CurrentCompany.Id, value);
            return Redirect("/");
        }

        [Route("user/vote")]
        [IsCompany]
        public ActionResult VoteUserView(int userId)
        {
            var viewModel = new VotesVoteUserViewViewModel();
            viewModel.UserId = userId;
            if (CurrentCompany != null)
                if (this.votes.CompanyVotedUser(CurrentCompany.Id, userId))
                {
                    viewModel.Voted = true;
                    viewModel.Value = this.votes.GetUserVote(CurrentCompany.Id, userId).Value;
                }
                else
                {
                    viewModel.Voted = false;
                }
            return PartialView(viewModel);
        }
    }
}