﻿using RayProfile.Data;
using RayProfile.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace RayProfile.Service
{
    public interface IExperienceService
    {
        /// <summary>
        /// Add an experience
        /// </summary>
        /// <param name="experience">The experience to be added</param>
        void Add(Experience experience);

        /// <summary>
        /// Check if an experience belongs to a user
        /// </summary>
        /// <param name="experienceId">The id of an experience</param>
        /// <param name="userId">The id of a user</param>
        /// <returns></returns>
        bool BelongsTo(int experienceId, int userId);

        /// <summary>
        /// Delete an experience
        /// </summary>
        /// <param name="id">The id of the experience to be deleted</param>
        void Delete(int id);

        /// <summary>
        /// Edit an experience
        /// </summary>
        /// <param name="experience">The experience to be edited</param>
        void Edit(Experience experience);

        /// <summary>
        /// Get an experience by id
        /// </summary>
        /// <param name="id">The id of an experience</param>
        /// <returns></returns>
        Experience Get(int id);

        /// <summary>
        /// Get experience for user
        /// </summary>
        /// <param name="userId">The id of a user</param>
        /// <returns></returns>
        List<Experience> GetForUser(int userId);
    }

    public class ExperienceService : IExperienceService
    {
        private AppContext db = new AppContext();

        public void Add(Experience experience)
        {
            db.Experience.Add(experience);
            db.SaveChanges();
        }

        public bool BelongsTo(int experienceId, int userId)
        {
            return db.Experience.Include(x => x.User).Any(x => x.Id == experienceId && x.User.Id == userId);
        }

        public void Delete(int id)
        {
            var experience = db.Experience.Find(id);
            db.Experience.Remove(experience);
            db.SaveChanges();
        }

        public void Edit(Experience experience)
        {
            db.Entry(experience).State = EntityState.Modified;
            db.SaveChanges();
        }

        public Experience Get(int id)
        {
            return db.Experience.Find(id);
        }

        public List<Experience> GetForUser(int userId)
        {
            return db.Experience
                .Include(x => x.User)
                .Where(x => x.User.Id == userId)
                .OrderByDescending(x => x.StartYear)
                .ToList();
        }
    }
}