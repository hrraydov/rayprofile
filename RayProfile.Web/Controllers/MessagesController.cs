﻿using RayProfile.Service;
using RayProfile.Web.Filters;
using RayProfile.Web.ViewModels;
using System.Collections.Generic;
using System.Web.Mvc;

namespace RayProfile.Web.Controllers
{
    public class MessagesController : BaseController
    {
        public MessagesController()
            : base()
        {
        }

        public MessagesController(IMessageService messageService)
        {
            this.messages = messageService;
        }

        [Route("user/messages")]
        [HttpGet]
        [IsUser]
        public ActionResult Received()
        {
            var messages = this.messages.GetForUser(CurrentUser.Id);
            var viewModel = new List<MessagesReceivedViewModel>();
            foreach (var item in messages)
            {
                viewModel.Add(new MessagesReceivedViewModel
                {
                    Content = item.Content,
                });
            }
            return View(viewModel);
        }
    }
}