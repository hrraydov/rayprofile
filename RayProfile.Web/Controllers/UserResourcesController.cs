﻿using RayProfile.Models;
using RayProfile.Service;
using RayProfile.Web.Filters;
using RayProfile.Web.ViewModels;
using System.Collections.Generic;
using System.Web.Mvc;

namespace RayProfile.Web.Controllers
{
    [IsUser]
    public class UserResourcesController : BaseController
    {
        public UserResourcesController()
            : base()
        {
        }

        public UserResourcesController(IResourceService resourceService)
        {
            this.resources = resourceService;
        }

        [Route("user/resource/add")]
        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }

        [Route("user/resource/add")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(UserResourcesAddViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                this.resources.Add(new Resource
                {
                    Content = viewModel.Content,
                    Importance = viewModel.Importance,
                    Link = viewModel.Link,
                    Title = viewModel.Title,
                    UserId = CurrentUser.Id
                });
                return Redirect("/user/resources");
            }
            return View(viewModel);
        }

        [Route("user/resource/delete/{id}")]
        [HttpGet]
        public ActionResult Delete(int id)
        {
            if (this.belongsToCurrentUser(id))
            {
                this.resources.Delete(id);
            }
            return Redirect("/user/resources");
        }

        [Route("user/resource/edit/{id}")]
        [HttpGet]
        public ActionResult Edit(int id)
        {
            if (this.belongsToCurrentUser(id))
            {
                var resource = this.resources.Get(id);
                var viewModel = new UserResourcesEditViewModel
                {
                    Id = resource.Id,
                    Content = resource.Content,
                    Importance = resource.Importance,
                    Link = resource.Link,
                    Title = resource.Title,
                };
                return View(viewModel);
            }
            else
            {
                return Redirect("/user/resources");
            }
        }

        [Route("user/resource/edit/{id}")]
        [HttpPost]
        public ActionResult Edit(UserResourcesEditViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                if (this.belongsToCurrentUser(viewModel.Id))
                {
                    var resource = new Resource
                    {
                        Id = viewModel.Id,
                        Content = viewModel.Content,
                        Importance = viewModel.Importance,
                        Link = viewModel.Link,
                        Title = viewModel.Title,
                        UserId = CurrentUser.Id,
                    };
                    this.resources.Edit(resource);
                }
                return Redirect("/user/resources");
            }
            return View(viewModel);
        }

        [Route("user/resources")]
        [HttpGet]
        public ActionResult Index()
        {
            var resources = this.resources.GetForUser(CurrentUser.Id);
            var viewModel = new List<UserResourcesIndexViewModel>();
            foreach (var item in resources)
            {
                viewModel.Add(new UserResourcesIndexViewModel
                {
                    Content = item.Content,
                    Id = item.Id,
                    Link = item.Link,
                    Title = item.Title
                });
            }
            return View(viewModel);
        }

        private bool belongsToCurrentUser(int id)
        {
            return this.resources.BelongsTo(id, CurrentUser.Id);
        }
    }
}