﻿using RayProfile.Service;
using RayProfile.Web.Filters;
using RayProfile.Web.ViewModels;
using System.Web.Mvc;

namespace RayProfile.Web.Controllers
{
    [IsUser]
    public class UserDetailsController : BaseController
    {
        public UserDetailsController()
            : base()
        {
        }

        public UserDetailsController(IUserService userService)
        {
            this.users = userService;
        }

        [Route("user/details")]
        [HttpGet]
        public ActionResult Edit()
        {
            var viewmodel = new UserDetailsEditViewModel
            {
                About = CurrentUser.Details.About,
                Country = CurrentUser.Details.Country,
                FatherName = CurrentUser.Details.FatherName,
                FirstName = CurrentUser.Details.FirstName,
                Sex = CurrentUser.Details.Sex,
                Surname = CurrentUser.Details.Surname,
                Town = CurrentUser.Details.Town,
            };
            return View(viewmodel);
        }

        [Route("user/details")]
        [HttpPost]
        public ActionResult Edit(UserDetailsEditViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var user = CurrentUser;
                user.Details.About = viewModel.About;
                user.Details.Country = viewModel.Country;
                user.Details.FatherName = viewModel.FatherName;
                user.Details.FirstName = viewModel.FirstName;
                user.Details.Sex = viewModel.Sex;
                user.Details.Surname = viewModel.Surname;
                user.Details.Town = viewModel.Town;

                this.users.Edit(user);
                return Redirect("/user/details");
            }
            return View(viewModel);
        }
    }
}