﻿using RayProfile.Data;
using RayProfile.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace RayProfile.Service
{
    public interface IMessageService
    {
        /// <summary>
        /// Add a message
        /// </summary>
        /// <param name="message">The message to be added</param>
        void Add(Message message);

        /// <summary>
        /// get messages for user
        /// </summary>
        /// <param name="userId">The id of a user</param>
        /// <returns></returns>
        List<Message> GetForUser(int userId);
    }

    public class MessageService : IMessageService
    {
        private AppContext db = new AppContext();

        public void Add(Message message)
        {
            db.Messages.Add(message);
            db.SaveChanges();
        }

        public List<Message> GetForUser(int userId)
        {
            return db.Messages.Include(x => x.User).Where(x => x.User.Id == userId).OrderByDescending(x => x.Id).ToList();
        }
    }
}