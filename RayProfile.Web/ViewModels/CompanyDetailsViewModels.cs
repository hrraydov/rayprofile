﻿using RayProfile.Models;
using System.ComponentModel.DataAnnotations;

namespace RayProfile.Web.ViewModels
{
    public class CompanyDetailsEditViewModel
    {
        [Display(
            Name = "Desc",
            ResourceType = typeof(Resources.ViewModels.CompanyDetails)
            )]
        public string Desc { get; set; }

        [Display(
            Name = "Founded",
            ResourceType = typeof(Resources.ViewModels.CompanyDetails)
            )]
        public short? Founded { get; set; }

        [Display(
            Name = "Industry",
            ResourceType = typeof(Resources.ViewModels.CompanyDetails)
            )]
        public string Industry { get; set; }

        [Display(
            Name = "Site",
            ResourceType = typeof(Resources.ViewModels.CompanyDetails)
            )]
        [Url]
        public string Site { get; set; }

        [Display(
            Name = "Size",
            ResourceType = typeof(Resources.ViewModels.CompanyDetails)
            )]
        public CompanySize? Size { get; set; }

        public string SizeStr { get; set; }
    }
}