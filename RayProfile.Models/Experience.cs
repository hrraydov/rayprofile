﻿using System.ComponentModel.DataAnnotations;

namespace RayProfile.Models
{
    public class Experience
    {
        [Required]
        [StringLength(150)]
        public string CompanyName { get; set; }

        public short? EndYear { get; set; }

        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(150)]
        public string Position { get; set; }

        [Required]
        public short StartYear { get; set; }

        public User User { get; set; }

        [Required]
        public int UserId { get; set; }
    }
}