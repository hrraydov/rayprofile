﻿using System.ComponentModel.DataAnnotations;

namespace RayProfile.Web.ViewModels
{
    public class UserResourcesAddViewModel
    {
        [Display(
            Name = "Content",
            ResourceType = typeof(Resources.ViewModels.UserResources)
            )]
        public string Content { get; set; }

        [Required]
        [Range(1, 100)]
        [Display(
            Name = "Importance",
            ResourceType = typeof(Resources.ViewModels.UserResources)
            )]
        public int Importance { get; set; }

        [Display(
            Name = "Link",
            ResourceType = typeof(Resources.ViewModels.UserResources)
            )]
        [Url]
        public string Link { get; set; }

        [Required]
        [Display(
            Name = "Title",
            ResourceType = typeof(Resources.ViewModels.UserResources)
            )]
        public string Title { get; set; }
    }

    public class UserResourcesEditViewModel
    {
        [Display(
            Name = "Content",
            ResourceType = typeof(Resources.ViewModels.UserResources)
            )]
        public string Content { get; set; }

        public int Id { get; set; }

        [Required]
        [Range(1, 100)]
        [Display(
            Name = "Importance",
            ResourceType = typeof(Resources.ViewModels.UserResources)
            )]
        public int Importance { get; set; }

        [Display(
            Name = "Link",
            ResourceType = typeof(Resources.ViewModels.UserResources)
            )]
        [Url]
        public string Link { get; set; }

        [Required]
        [Display(
            Name = "Title",
            ResourceType = typeof(Resources.ViewModels.UserResources)
            )]
        public string Title { get; set; }
    }

    public class UserResourcesIndexViewModel
    {
        public string Content { get; set; }

        public int Id { get; set; }

        public string Link { get; set; }

        public string Title { get; set; }
    }
}