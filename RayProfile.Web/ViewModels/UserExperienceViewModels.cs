﻿using System.ComponentModel.DataAnnotations;

namespace RayProfile.Web.ViewModels
{
    public class UserExperienceAddViewModel
    {
        [Required(
            ErrorMessageResourceName = "CompanyNameRequired",
            ErrorMessageResourceType = typeof(Resources.ViewModels.UserExperience)
            )]
        [Display(
            Name = "CompanyName",
            ResourceType = typeof(Resources.ViewModels.UserExperience)
            )]
        public string CompanyName { get; set; }

        public string CompanyNamesJSON { get; set; }

        [Display(
            Name = "EndYear",
            ResourceType = typeof(Resources.ViewModels.UserExperience)
            )]
        public short? EndYear { get; set; }

        [Required(
            ErrorMessageResourceName = "PositionRequired",
            ErrorMessageResourceType = typeof(Resources.ViewModels.UserExperience)
            )]
        [Display(
            Name = "Position",
            ResourceType = typeof(Resources.ViewModels.UserExperience)
            )]
        public string Position { get; set; }

        [Required(
            ErrorMessageResourceName = "StartYearRequired",
            ErrorMessageResourceType = typeof(Resources.ViewModels.UserExperience)
            )]
        [Display(
            Name = "StartYear",
            ResourceType = typeof(Resources.ViewModels.UserExperience)
            )]
        public short StartYear { get; set; }
    }

    public class UserExperienceEditViewModel
    {
        [Required(
            ErrorMessageResourceName = "CompanyNameRequired",
            ErrorMessageResourceType = typeof(Resources.ViewModels.UserExperience)
            )]
        [Display(
            Name = "CompanyName",
            ResourceType = typeof(Resources.ViewModels.UserExperience)
            )]
        public string CompanyName { get; set; }

        public string CompanyNamesJSON { get; set; }

        [Display(
            Name = "EndYear",
            ResourceType = typeof(Resources.ViewModels.UserExperience)
            )]
        public short? EndYear { get; set; }

        public int Id { get; set; }

        [Required(
            ErrorMessageResourceName = "PositionRequired",
            ErrorMessageResourceType = typeof(Resources.ViewModels.UserExperience)
            )]
        [Display(
            Name = "Position",
            ResourceType = typeof(Resources.ViewModels.UserExperience)
            )]
        public string Position { get; set; }

        [Required(
            ErrorMessageResourceName = "StartYearRequired",
            ErrorMessageResourceType = typeof(Resources.ViewModels.UserExperience)
            )]
        [Display(
            Name = "StartYear",
            ResourceType = typeof(Resources.ViewModels.UserExperience)
            )]
        public short StartYear { get; set; }
    }

    public class UserExperienceIndexViewModel
    {
        public bool CompanyExists { get; set; }

        public int CompanyId { get; set; }

        public string CompanyName { get; set; }

        public short? EndYear { get; set; }

        public int Id { get; set; }

        public string Position { get; set; }

        public short StartYear { get; set; }
    }
}