﻿using RayProfile.Data;
using RayProfile.Models;

namespace RayProfile.Service
{
    public interface ICompanyDetailsService
    {
        /// <summary>
        /// Give the string representation of CompanySize field
        /// </summary>
        /// <param name="size">The sise</param>
        /// <returns></returns>
        string CompanySizeToString(CompanySize? size);
    }

    public class CompanyDetailsService : ICompanyDetailsService
    {
        private AppContext db = new AppContext();

        public string CompanySizeToString(CompanySize? size)
        {
            string res = "";
            switch (size)
            {
                case null: res = ""; break;
                case CompanySize.OneToTen: res = "1-10"; break;
                case CompanySize.TenToFifty: res = "10-50"; break;
                case CompanySize.FiftyToHundred: res = "51-100"; break;
                case CompanySize.HundredToFiveH: res = "101-500"; break;
                case CompanySize.FiveHToThousand: res = "501-1000"; break;
                case CompanySize.ThousandToFiveT: res = "1001-5000"; break;
                case CompanySize.FiveTToTenT: res = "5001-10000"; break;
                case CompanySize.OverTenT: res = "10001+"; break;
            }
            return res;
        }
    }
}