﻿using System.ComponentModel.DataAnnotations;

namespace RayProfile.Web.ViewModels
{
    public class UserDetailsEditViewModel
    {
        [Display(
            Name = "About",
            ResourceType = typeof(Resources.ViewModels.UserDetails)
            )]
        public string About { get; set; }

        [Display(
            Name = "Country",
            ResourceType = typeof(Resources.ViewModels.UserDetails)
            )]
        public string Country { get; set; }

        [Display(
            Name = "FatherName",
            ResourceType = typeof(Resources.ViewModels.UserDetails)
            )]
        public string FatherName { get; set; }

        [Display(
            Name = "FirstName",
            ResourceType = typeof(Resources.ViewModels.UserDetails)
            )]
        public string FirstName { get; set; }

        [Display(
            Name = "Sex",
            ResourceType = typeof(Resources.ViewModels.UserDetails)
            )]
        public int Sex { get; set; }

        [Display(
            Name = "Surname",
            ResourceType = typeof(Resources.ViewModels.UserDetails)
            )]
        public string Surname { get; set; }

        [Display(
            Name = "Town",
            ResourceType = typeof(Resources.ViewModels.UserDetails)
            )]
        public string Town { get; set; }
    }
}