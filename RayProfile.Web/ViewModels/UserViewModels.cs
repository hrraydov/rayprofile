﻿using RayProfile.Models;
using System.ComponentModel.DataAnnotations;

namespace RayProfile.Web.ViewModels
{
    public class UserBestViewModel
    {
        public double AverageVote { get; set; }

        public int Id { get; set; }

        public string Name { get; set; }

        public int VotesCount { get; set; }
    }

    public class UserEducationViewModel
    {
        public short? EndYear { get; set; }

        public string SchoolName { get; set; }

        public short StartYear { get; set; }

        public string Type { get; set; }
    }

    public class UserExperienceViewModel
    {
        public bool CompanyExists { get; set; }

        public int CompanyId { get; set; }

        public string CompanyName { get; set; }

        public short? EndYear { get; set; }

        public string Position { get; set; }

        public short StartYear { get; set; }
    }

    public class UserLoginViewModel
    {
        [Required(
            ErrorMessageResourceName = "PasswordRequired",
            ErrorMessageResourceType = typeof(Resources.ViewModels.User)
            )]
        [Display(
            Name = "Password",
            ResourceType = typeof(Resources.ViewModels.User)
            )]
        public string Password { get; set; }

        [Required(
            ErrorMessageResourceName = "UsernameRequired",
            ErrorMessageResourceType = typeof(Resources.ViewModels.User)
            )]
        [Display(
            Name = "Username",
            ResourceType = typeof(Resources.ViewModels.User)
            )]
        public string Username { get; set; }
    }

    public class UserRegisterViewModel
    {
        [Required(
            ErrorMessageResourceName = "EmailRequired",
            ErrorMessageResourceType = typeof(Resources.ViewModels.User)
            )]
        [EmailAddress(
            ErrorMessage = null,
            ErrorMessageResourceName = "EmailValid",
            ErrorMessageResourceType = typeof(Resources.ViewModels.User)
            )]
        [Display(
            Name = "Email",
            ResourceType = typeof(Resources.ViewModels.User)
            )]
        public string Email { get; set; }

        public string Message { get; set; }

        [Required(
            ErrorMessageResourceName = "PasswordRequired",
            ErrorMessageResourceType = typeof(Resources.ViewModels.User)
            )]
        [DataType(DataType.Password)]
        [StringLength(
            150,
            MinimumLength = 4,
            ErrorMessageResourceName = "PasswordLength",
            ErrorMessageResourceType = typeof(Resources.ViewModels.User))]
        [Display(
            Name = "Password",
            ResourceType = typeof(Resources.ViewModels.User)
            )]
        public string Password { get; set; }

        [Compare(
            "Password",
            ErrorMessageResourceName = "PasswordsMatch",
            ErrorMessageResourceType = typeof(Resources.ViewModels.User))]
        [DataType(DataType.Password)]
        [Display(
            Name = "PasswordConfirm",
            ResourceType = typeof(Resources.ViewModels.User)
            )]
        public string PasswordConfirm { get; set; }

        [Required(
            ErrorMessageResourceName = "UsernameRequired",
            ErrorMessageResourceType = typeof(Resources.ViewModels.User)
            )]
        [StringLength(
            150,
            MinimumLength = 4,
            ErrorMessageResourceName = "UsernameLength",
            ErrorMessageResourceType = typeof(Resources.ViewModels.User))]
        [Display(
            Name = "Username",
            ResourceType = typeof(Resources.ViewModels.User)
            )]
        public string Username { get; set; }
    }

    public class UserResourcesViewModel
    {
        public string Content { get; set; }

        public string Link { get; set; }

        public string Title { get; set; }
    }

    public class UserShowViewModel
    {
        public UserDetails Details { get; set; }

        public int Id { get; set; }

        public string Name { get; set; }
    }

    public class UserSkillsViewModel
    {
        public int PageCount { get; set; }

        public int UserId { get; set; }
    }
}