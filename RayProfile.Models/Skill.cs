﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RayProfile.Models
{
    public class Skill
    {
        private ICollection<PositionSkill> offerSkills;

        private ICollection<UserSkill> userSkills;

        public Skill()
        {
            this.userSkills = new HashSet<UserSkill>();
            this.offerSkills = new HashSet<PositionSkill>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(200)]
        public string Name { get; set; }

        public virtual ICollection<PositionSkill> OfferSkills
        {
            get { return this.offerSkills; }
            set { this.offerSkills = value; }
        }

        public virtual ICollection<UserSkill> UserSkills
        {
            get { return this.userSkills; }
            set { this.userSkills = value; }
        }
    }
}