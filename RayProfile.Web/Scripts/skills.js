﻿$(document).ready(function () {
    var userId = $("[name=userId]").val();
    $.ajax({
        url: "/api/skills/" + userId,
        type: "GET",
        success: function (data) {
            var html = "";
            for (var i = 0; i < data.length; i++) {
                html += '<div class="col-sm-3 skill-panel panel panel-default"><div><h4>' + data[i].Name + '</h4><h5>' + data[i].Value + ' / 10</h5></div></div>';
            }
            $(".skillrow").html(html);
        }
    });
    $(".page-button").on("click", function () {
        var page = $(this).attr("data-page");
        $.ajax({
            url: "/api/skills/" + userId + "/" + page,
            type: "GET",
            success: function (data) {
                var html = "";
                for (var i = 0; i < data.length; i++) {
                    html += '<div class="col-sm-3 skill-panel panel panel-default"><div><h4>' + data[i].Name + '</h4><h5>' + data[i].Value + ' / 10</h5></div></div>';
                }
                $(".skillrow").html(html);
            }
        });
    });
});