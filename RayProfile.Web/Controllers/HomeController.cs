﻿using RayProfile.Models;
using RayProfile.Service;
using RayProfile.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace RayProfile.Web.Controllers
{
    public class HomeController : BaseController
    {
        public HomeController()
            : base()
        {
        }

        public HomeController(IUserService userService, ICompanyService companyService)
        {
            this.users = userService;
            this.companies = companyService;
        }

        public ActionResult Index()
        {
            return View();
        }

        [Route("lang/{lang}")]
        [HttpGet]
        public ActionResult Lang(string lang, string returnUrl)
        {
            Response.Cookies["_lang"].Value = lang;
            return Redirect(returnUrl);
        }

        [Route("search")]
        [HttpGet]
        public ActionResult Search(string q)
        {
            if (String.IsNullOrEmpty(q))
                q = "";
            q = q.ToLower();
            List<User> users = new List<User>();
            List<Company> companies = new List<Company>();
            users = this.users.GetAll()
                .Where(x => x.Username.ToLower().Contains(q) ||
                    (x.Details.FirstName != null && x.Details.FirstName.ToLower().Contains(q)) ||
                    (x.Details.FatherName != null && x.Details.FatherName.ToLower().Contains(q)) ||
                    (x.Details.Surname != null && x.Details.Surname.ToLower().Contains(q)))
                .OrderBy(x => x.Username)
                .ToList();
            companies = this.companies.GetAll()
                .Where(x => x.Name.ToLower().Contains(q) || x.LoginName.ToLower().Contains(q))
                .OrderBy(x => x.LoginName)
                .ToList();
            var serializer = new JavaScriptSerializer();
            var viewModel = new HomeSearchViewModel
            {
                Companies = companies,
                Users = users,
                UsersJSON = serializer.Serialize(users.Select(x => new { Username = x.Username, Id = x.Id })),
                CompaniesJSON = serializer.Serialize(companies.Select(x => new { Name = x.Name, Id = x.Id })),
            };
            return View(viewModel);
        }
    }
}