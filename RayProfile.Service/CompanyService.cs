﻿using RayProfile.Data;
using RayProfile.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace RayProfile.Service
{
    public interface ICompanyService
    {/// <summary>
        /// Checks if company exists by given name, login name, email
        /// </summary>
        /// <param name="loginName">Login name</param>
        /// <param name="name">Name</param>
        /// <param name="email">Email</param>
        /// <returns></returns>
        bool CompanyLoginOrNameOrEmailExists(string loginName, string name, string email);

        /// <summary>
        /// Edit a company
        /// </summary>
        /// <param name="company">The company to be edited</param>
        void Edit(Company company);

        /// <summary>
        /// Checks if company exists by given login name
        /// </summary>
        /// <param name="loginName">Login name</param>
        /// <returns></returns>
        bool Exists(string loginName);

        /// <summary>
        /// Chck if company exists
        /// </summary>
        /// <param name="name">The name of a company</param>
        /// <returns></returns>
        bool ExistsByName(string name);

        /// <summary>
        /// Get all companies
        /// </summary>
        /// <returns></returns>
        List<Company> GetAll();

        /// <summary>
        /// Get the names of all companies
        /// </summary>
        /// <returns></returns>
        List<string> GetAllNames();

        /// <summary>
        /// Get the top ten companies ordered by its rating
        /// </summary>
        /// <returns></returns>
        List<Company> GetBest();

        /// <summary>
        /// Get a company by id
        /// </summary>
        /// <param name="id">The id of a company</param>
        /// <returns></returns>
        Company GetById(int id);

        /// <summary>
        /// Gets a company by given login name
        /// </summary>
        /// <param name="loginName">Login name</param>
        /// <returns></returns>
        Company GetByLoginName(string loginName);

        /// <summary>
        /// get a company by name
        /// </summary>
        /// <param name="name">The name of a company</param>
        /// <returns></returns>
        Company GetByName(string name);

        /// <summary>
        /// Registers company
        /// </summary>
        /// <param name="company">Company</param>
        void Register(Company company);
    }

    public class CompanyService : ICompanyService
    {
        private AppContext db = new AppContext();

        public bool CompanyLoginOrNameOrEmailExists(string loginName, string name, string email)
        {
            return db.Companies.Any(x => x.Email == email || x.LoginName == loginName || x.Name == name);
        }

        public void Edit(Company company)
        {
            db.Entry(company).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public bool Exists(string loginName)
        {
            return db.Companies.Any(x => x.LoginName == loginName);
        }

        public bool ExistsByName(string name)
        {
            return db.Companies.Any(x => x.Name == name);
        }

        public List<Company> GetAll()
        {
            return db.Companies.ToList();
        }

        public List<string> GetAllNames()
        {
            return db.Companies.Select(x => x.Name).ToList();
        }

        public List<Company> GetBest()
        {
            return db.Companies
                .Include(x => x.CompanyVotes)
                .Where(x => x.CompanyVotes.Select(y => y.Value).Average() > 0)
                .OrderByDescending(x => x.CompanyVotes.Select(y => y.Value).Average())
                .ThenByDescending(x => x.CompanyVotes.Select(y => y.Value).Count())
                .Take(10)
                .ToList();
        }

        public Company GetById(int id)
        {
            return db.Companies.Find(id);
        }

        public Company GetByLoginName(string loginName)
        {
            return db.Companies.First(x => x.LoginName == loginName);
        }

        public Company GetByName(string name)
        {
            return db.Companies.First(x => x.Name == name);
        }

        public void Register(Company company)
        {
            db.Companies.Add(company);
            db.SaveChanges();
        }
    }
}