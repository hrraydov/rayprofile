﻿using System.ComponentModel.DataAnnotations;

namespace RayProfile.Models
{
    public class Message
    {
        [Required]
        public string Content { get; set; }

        [Key]
        public int Id { get; set; }

        public virtual User User { get; set; }

        [Required]
        public int UserId { get; set; }
    }
}