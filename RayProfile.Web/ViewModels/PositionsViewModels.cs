﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RayProfile.Web.ViewModels
{
    public class PositionsAddViewModel
    {
        [Required]
        [Display(
            Name = "Desc",
            ResourceType = typeof(Resources.ViewModels.Positions)
            )]
        public string Desc { get; set; }

        [Required]
        [Display(
            Name = "Name",
            ResourceType = typeof(Resources.ViewModels.Positions)
            )]
        public string Name { get; set; }

        [Display(
            Name = "Skills",
            ResourceType = typeof(Resources.Common)
            )]
        public List<PositionSkillModel> Skills { get; set; }

        public string SkillsJSON { get; set; }
    }

    public class PositionsEditViewModel
    {
        [Required]
        [Display(
            Name = "Desc",
            ResourceType = typeof(Resources.ViewModels.Positions)
            )]
        public string Desc { get; set; }

        public int Id { get; set; }

        [Required]
        [Display(
            Name = "Name",
            ResourceType = typeof(Resources.ViewModels.Positions)
            )]
        public string Name { get; set; }

        [Display(
            Name = "Skills",
            ResourceType = typeof(Resources.Common)
            )]
        public List<PositionSkillModel> Skills { get; set; }

        public string SkillsJSON { get; set; }

        public string ValuesJSON { get; set; }
    }

    public class PositionsIndexViewModel
    {
        public string Desc { get; set; }

        public int Id { get; set; }

        public string Name { get; set; }

        public List<PositionSkillModel> Skills { get; set; }
    }

    public class PositionSkillModel
    {
        public string Name { get; set; }

        public int? Value { get; set; }
    }
}