﻿using RayProfile.Models;
using System.Data.Entity;

namespace RayProfile.Data
{
    public class AppContext : DbContext
    {
        public AppContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<Application> Applications { get; set; }

        public DbSet<Company> Companies { get; set; }

        public DbSet<CompanyVote> CompanyVotes { get; set; }

        public DbSet<Education> Education { get; set; }

        public DbSet<Experience> Experience { get; set; }

        public DbSet<Message> Messages { get; set; }

        public DbSet<Position> Positions { get; set; }

        public DbSet<PositionSkill> PositionSkills { get; set; }

        public DbSet<Product> Products { get; set; }

        public DbSet<Resource> Resources { get; set; }

        public DbSet<Skill> Skills { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<UserSkill> UsersSkills { get; set; }

        public DbSet<UserVote> UserVotes { get; set; }
    }
}