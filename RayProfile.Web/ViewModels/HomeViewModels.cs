﻿using RayProfile.Models;
using System.Collections.Generic;

namespace RayProfile.Web.ViewModels
{
    public class HomeSearchViewModel
    {
        public List<Company> Companies { get; set; }

        public string CompaniesJSON { get; set; }

        public List<User> Users { get; set; }

        public string UsersJSON { get; set; }
    }
}