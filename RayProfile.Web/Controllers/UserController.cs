﻿using RayProfile.Models;
using RayProfile.Service;
using RayProfile.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Security;

namespace RayProfile.Web.Controllers
{
    public class UserController : BaseController
    {
        public UserController()
            : base()
        {
        }

        public UserController(IUserService userService, IEducationService educationService, IExperienceService experienceService, IResourceService resourceService, ISkillService skillService)
        {
            this.users = userService;
            this.education = educationService;
            this.experience = experienceService;
            this.resources = resourceService;
            this.skills = skillService;
        }

        [Route("user/best")]
        public ActionResult Best()
        {
            var users = this.users.GetBest();
            var viewModel = new List<UserBestViewModel>();
            foreach (var item in users)
            {
                var values = item.UserVotes.Select(x => x.Value).ToList();
                if (values.Count > 0)
                {
                    viewModel.Add(new UserBestViewModel
                    {
                        AverageVote = values.Average(),
                        Id = item.Id,
                        Name = item.Username,
                        VotesCount = values.Count
                    });
                }
            }
            return PartialView(viewModel);
        }

        [Route("education")]
        public ActionResult Education(int userId)
        {
            var education = this.education.GetByUserIdOrderByDesc(userId);
            var viewModel = new List<UserEducationViewModel>();
            foreach (var item in education)
            {
                viewModel.Add(new UserEducationViewModel
                {
                    EndYear = item.EndYear,
                    SchoolName = item.SchoolName,
                    StartYear = item.StartYear,
                    Type = this.education.GetEducationType(item.Type),
                });
            }
            return PartialView(viewModel);
        }

        [Route("experience")]
        public ActionResult Experience(int userId)
        {
            var experience = this.experience.GetForUser(userId);
            var viewModel = new List<UserExperienceViewModel>();
            foreach (var item in experience)
            {
                viewModel.Add(new UserExperienceViewModel
                {
                    CompanyExists = this.companies.ExistsByName(item.CompanyName),
                    CompanyName = item.CompanyName,
                    EndYear = item.EndYear,
                    Position = item.Position,
                    StartYear = item.StartYear,
                });
            }
            foreach (var item in viewModel)
            {
                if (item.CompanyExists)
                {
                    item.CompanyId = this.companies.GetByName(item.CompanyName).Id;
                }
            }
            return PartialView(viewModel);
        }

        [Route("login/user")]
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [Route("login/user")]
        [HttpPost]
        public ActionResult Login(UserLoginViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                viewModel.Username = viewModel.Username.Trim();
                viewModel.Password = viewModel.Password.Trim();
                if (!this.users.UserExists(viewModel.Username))
                {
                    ModelState.AddModelError("", RayProfile.Resources.Controllers.User.UserDoesNotExists);
                }
                if (ModelState.IsValid)
                {
                    var user = this.users.GetByName(viewModel.Username);
                    if (!Crypto.VerifyHashedPassword(user.Password, viewModel.Password))
                    {
                        ModelState.AddModelError("", RayProfile.Resources.Controllers.User.UserDoesNotExists);
                    }
                    if (ModelState.IsValid)
                    {
                        FormsAuthentication.SetAuthCookie(viewModel.Username, false);
                        Session["type"] = "user";
                        return Redirect("/");
                    }
                    return View(viewModel);
                }
                return View(viewModel);
            }
            return View(viewModel);
        }

        [Route("logout")]
        [HttpGet]
        [Authorize]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session["type"] = null;
            return Redirect("/");
        }

        [Route("register/user")]
        [HttpGet]
        public ActionResult Register()
        {
            var viewModel = new UserRegisterViewModel
            {
                Message = ""
            };
            return View(viewModel);
        }

        [Route("register/user")]
        [HttpPost]
        public ActionResult Register(UserRegisterViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                viewModel.Username = viewModel.Username.Trim();
                viewModel.Email = viewModel.Email.Trim();
                viewModel.Password = viewModel.Password.Trim();
                viewModel.PasswordConfirm = viewModel.PasswordConfirm.Trim();
                if (this.users.UserOrEmailExists(viewModel.Username, viewModel.Email))
                {
                    ModelState.AddModelError("", RayProfile.Resources.Controllers.User.UserOrEmailExists);
                    return View(viewModel);
                }
                var password = viewModel.Password;

                this.users.Register(new RayProfile.Models.User
                {
                    Username = viewModel.Username,
                    Password = Crypto.HashPassword(password),
                    Email = viewModel.Email,
                    Details = new UserDetails(),
                });
                viewModel.Message = RayProfile.Resources.Controllers.User.RegisteredSuccessfully;
                return View(viewModel);
            }
            viewModel.Message = "";
            return View(viewModel);
        }

        [Route("resources")]
        public ActionResult Resources(int userId)
        {
            var resources = this.resources.GetForUser(userId);
            var viewModel = new List<UserResourcesViewModel>();
            foreach (var item in resources)
            {
                viewModel.Add(new UserResourcesViewModel
                {
                    Content = item.Content,
                    Link = item.Link,
                    Title = item.Title
                });
            }
            return PartialView(viewModel);
        }

        [Route("user/{id}")]
        [HttpGet]
        public ActionResult Show(int id)
        {
            var user = this.users.GetById(id);
            var viewModel = new UserShowViewModel
            {
                Id = user.Id,
                Name = user.Username,
                Details = user.Details
            };
            return View(viewModel);
        }

        [Route("skills")]
        public ActionResult Skills(int userId)
        {
            var skillsCount = this.skills.GetByUserId(userId).Count;
            var viewModel = new UserSkillsViewModel
            {
                PageCount = (int)Math.Ceiling((double)skillsCount / 4),
                UserId = userId,
            };
            return PartialView(viewModel);
        }
    }
}