﻿using RayProfile.Service;
using RayProfile.Web.ViewModels;
using System.Collections.Generic;
using System.Web.Mvc;

namespace RayProfile.Web.Controllers
{
    public class SkillsController : BaseController
    {
        public SkillsController()
            : base()
        {
        }

        public SkillsController(ISkillService skillService)
        {
            this.skills = skillService;
        }

        [Route("most-required-in-positions")]
        public ActionResult ShowMostRequired()
        {
            var skills = this.skills.MostUsedInPositions();
            var viewModel = new List<SkillsShowMostRequiredViewModel>();
            foreach (var item in skills)
            {
                viewModel.Add(new SkillsShowMostRequiredViewModel
                {
                    Name = item.Name,
                    Count = item.OfferSkills.Count,
                });
            }
            return PartialView(viewModel);
        }
    }
}