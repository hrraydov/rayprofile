﻿using RayProfile.Data;
using RayProfile.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace RayProfile.Service
{
    public interface IEducationService
    {
        /// <summary>
        /// Add an education
        /// </summary>
        /// <param name="education">Education to be added</param>
        void Add(Education education);

        /// <summary>
        /// Check if an education belongs to a user
        /// </summary>
        /// <param name="educationId">The id of an education</param>
        /// <param name="userId">The id of a user</param>
        /// <returns></returns>
        bool BelongsTo(int educationId, int userId);

        /// <summary>
        /// Delete an education
        /// </summary>
        /// <param name="educationId">The id of the education to be deleted</param>
        void Delete(Education education);

        /// <summary>
        /// Edit education
        /// </summary>
        /// <param name="education">The education to be edited</param>
        void Edit(Education education);

        /// <summary>
        /// Get education by id
        /// </summary>
        /// <param name="id">The id of an education</param>
        /// <returns></returns>
        Education GetById(int id);

        /// <summary>
        /// Get education by user id in descending order
        /// </summary>
        /// <param name="username">username</param>
        /// <returns>List of education</returns>
        List<Education> GetByUserIdOrderByDesc(int userId);

        /// <summary>
        /// Get education by user name in descending order
        /// </summary>
        /// <param name="username">username</param>
        /// <returns>List of education</returns>
        List<Education> GetByUserNameOrderByDesc(string username);

        /// <summary>
        /// Get the string representation of EducationType
        /// </summary>
        /// <param name="type">type</param>
        /// <returns></returns>
        string GetEducationType(EducationType type);
    }

    public class EducationService : IEducationService
    {
        private AppContext db = new AppContext();

        public void Add(Education education)
        {
            db.Education.Add(education);
            db.SaveChanges();
        }

        public bool BelongsTo(int educationId, int userId)
        {
            return db.Education.Include(x => x.User).Any(x => x.Id == educationId && x.User.Id == userId);
        }

        public void Delete(Education education)
        {
            db.Education.Remove(education);
            db.SaveChanges();
        }

        public void Edit(Education education)
        {
            db.Entry(education).State = EntityState.Modified;
            db.SaveChanges();
        }

        public Education GetById(int id)
        {
            return db.Education.Find(id);
        }

        public List<Education> GetByUserIdOrderByDesc(int userId)
        {
            return db.Education
                .Include(x => x.User)
                .Where(x => x.User.Id == userId)
                .OrderByDescending(x => x.StartYear)
                .ToList();
        }

        public List<Education> GetByUserNameOrderByDesc(string username)
        {
            return db.Education.Include(x => x.User)
                .Where(x => x.User.Username == username)
                .OrderByDescending(x => x.StartYear)
                .ToList();
        }

        public string GetEducationType(EducationType type)
        {
            string result = "";
            switch (type)
            {
                case EducationType.Elementary:
                    result = Resources.Common.Elementary;
                    break;

                case EducationType.Primary:
                    result = Resources.Common.Primary;
                    break;

                case EducationType.Secondary:
                    result = Resources.Common.Secondary;
                    break;

                case EducationType.University:
                    result = Resources.Common.University;
                    break;
            }
            return result;
        }
    }
}