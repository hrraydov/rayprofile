﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RayProfile.Models
{
    [ComplexType]
    public class UserDetails
    {
        [StringLength(1000)]
        public string About { get; set; }

        [StringLength(100)]
        public string Country { get; set; }

        [StringLength(100)]
        public string FatherName { get; set; }

        [StringLength(100)]
        public string FirstName { get; set; }
        /// <summary>
        /// 1 for male, 2 for female
        /// </summary>
        public int Sex { get; set; }

        [StringLength(100)]
        public string Surname { get; set; }

        [StringLength(100)]
        public string Town { get; set; }
    }
}