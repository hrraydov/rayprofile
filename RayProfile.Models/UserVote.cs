﻿using System.ComponentModel.DataAnnotations;

namespace RayProfile.Models
{
    public class UserVote
    {
        [Key]
        public int Id { get; set; }

        public virtual User User { get; set; }

        [Required]
        public int UserId { get; set; }

        [Required]
        public int Value { get; set; }

        public virtual Company VotedBy { get; set; }

        [Required]
        public int VotedById { get; set; }
    }
}