﻿using RayProfile.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RayProfile.Web.ViewModels
{
    public class CompanyBestViewModel
    {
        public double AverageVote { get; set; }

        public int Id { get; set; }

        public string Name { get; set; }

        public int VotesCount { get; set; }
    }

    public class CompanyLoginViewModel
    {
        [Required(
            ErrorMessageResourceName = "LoginNameRequired",
            ErrorMessageResourceType = typeof(Resources.ViewModels.Company)
            )]
        [Display(
            Name = "LoginName",
            ResourceType = typeof(Resources.ViewModels.Company)
            )]
        public string LoginName { get; set; }

        [Required(
            ErrorMessageResourceName = "PasswordRequired",
            ErrorMessageResourceType = typeof(Resources.ViewModels.Company)
            )]
        [Display(
            Name = "Password",
            ResourceType = typeof(Resources.ViewModels.Company)
            )]
        public string Password { get; set; }
    }

    public class CompanyPositionsViewModel
    {
        public User CurrentUser { get; set; }

        public string Desc { get; set; }

        public int Id { get; set; }

        public string Name { get; set; }

        public List<PositionSkillModel> Skills { get; set; }
    }

    public class CompanyProductsViewModel
    {
        public string Desc { get; set; }

        public string Name { get; set; }
    }

    public class CompanyRegisterViewModel
    {
        [Required(
            ErrorMessageResourceName = "Email",
            ErrorMessageResourceType = typeof(Resources.ViewModels.Company)
            )]
        [StringLength(
            150,
            MinimumLength = 4,
            ErrorMessageResourceName = "EmailLength",
            ErrorMessageResourceType = typeof(Resources.ViewModels.Company)
            )]
        [EmailAddress]
        [Display(
            Name = "Email",
            ResourceType = typeof(Resources.ViewModels.Company)
            )]
        public string Email { get; set; }

        [Required(
            ErrorMessageResourceName = "LoginNameRequired",
            ErrorMessageResourceType = typeof(Resources.ViewModels.Company)
            )]
        [StringLength(
            150,
            MinimumLength = 4,
            ErrorMessageResourceName = "LoginNameLength",
            ErrorMessageResourceType = typeof(Resources.ViewModels.Company)
            )]
        [Display(
            Name = "LoginName",
            ResourceType = typeof(Resources.ViewModels.Company)
            )]
        public string LoginName { get; set; }

        [Required(
            ErrorMessageResourceName = "Name",
            ErrorMessageResourceType = typeof(Resources.ViewModels.Company)
            )]
        [StringLength(
            150,
            MinimumLength = 4,
            ErrorMessageResourceName = "NameLength",
            ErrorMessageResourceType = typeof(Resources.ViewModels.Company)
            )]
        [Display(
            Name = "Name",
            ResourceType = typeof(Resources.ViewModels.Company)
            )]
        public string Name { get; set; }

        [Required(
            ErrorMessageResourceName = "Password",
            ErrorMessageResourceType = typeof(Resources.ViewModels.Company)
            )]
        [StringLength(
            150,
            MinimumLength = 4,
            ErrorMessageResourceName = "PasswordLength",
            ErrorMessageResourceType = typeof(Resources.ViewModels.Company)
            )]
        [Display(
            Name = "Password",
            ResourceType = typeof(Resources.ViewModels.Company)
            )]
        public string Password { get; set; }

        [Compare(
            "Password",
            ErrorMessageResourceName = "PasswordsNotMatch",
            ErrorMessageResourceType = typeof(Resources.ViewModels.Company)
            )]
        [Display(
            Name = "PasswordConfirm",
            ResourceType = typeof(Resources.ViewModels.Company)
            )]
        public string PasswordConfirm { get; set; }
    }

    public class CompanyShowViewModel
    {
        public string Desc { get; set; }

        public short? Founded { get; set; }

        public int Id { get; set; }

        public string Industry { get; set; }

        public string Name { get; set; }

        public string Site { get; set; }

        public string Size { get; set; }
    }
}