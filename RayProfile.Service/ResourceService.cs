﻿using RayProfile.Data;
using RayProfile.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace RayProfile.Service
{
    public interface IResourceService
    {
        /// <summary>
        /// Add new resource
        /// </summary>
        /// <param name="resource">The resource to be added</param>
        void Add(Resource resource);

        /// <summary>
        /// Check id a resource belongs to a user
        /// </summary>
        /// <param name="resourceId">The id of a resource</param>
        /// <param name="userId">The id of a user</param>
        /// <returns></returns>
        bool BelongsTo(int resourceId, int userId);

        /// <summary>
        /// Delete a resource
        /// </summary>
        /// <param name="id">The id of the resource to be deleted</param>
        void Delete(int id);

        /// <summary>
        /// Edit a resource
        /// </summary>
        /// <param name="resource">The resource to be edited</param>
        void Edit(Resource resource);

        /// <summary>
        /// Get a resource by id
        /// </summary>
        /// <param name="id">The id of a resource</param>
        /// <returns></returns>
        Resource Get(int id);

        /// <summary>
        /// Get resources for a user
        /// </summary>
        /// <param name="userId">The id of a user</param>
        /// <returns></returns>
        List<Resource> GetForUser(int userId);
    }

    public class ResourceService : IResourceService
    {
        private AppContext db = new AppContext();

        public void Add(Resource resource)
        {
            db.Resources.Add(resource);
            db.SaveChanges();
        }

        public bool BelongsTo(int resourceId, int userId)
        {
            return db.Resources.Include(x => x.User).Any(x => x.Id == resourceId && x.User.Id == userId);
        }

        public void Delete(int id)
        {
            var resource = db.Resources.Find(id);
            db.Resources.Remove(resource);
            db.SaveChanges();
        }

        public void Edit(Resource resource)
        {
            db.Entry(resource).State = EntityState.Modified;
            db.SaveChanges();
        }

        public Resource Get(int id)
        {
            return db.Resources.Find(id);
        }

        public List<Resource> GetForUser(int userId)
        {
            return db.Resources.Where(x => x.User.Id == userId).OrderByDescending(x => x.Importance).ToList();
        }
    }
}