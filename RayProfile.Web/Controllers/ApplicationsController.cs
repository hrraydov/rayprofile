﻿using RayProfile.Models;
using RayProfile.Service;
using RayProfile.Web.Filters;
using System.Web.Mvc;

namespace RayProfile.Web.Controllers
{
    public class ApplicationsController : BaseController
    {
        public ApplicationsController()
            : base()
        {
        }

        public ApplicationsController(IApplicationService applicationService, IMessageService messageService)
        {
            this.applications = applicationService;
            this.messages = messageService;
        }

        [Route("application/{id}/accept")]
        [HttpPost]
        [IsCompany]
        public ActionResult Accept(int id, string Message)
        {
            var application = this.applications.GetById(id);
            var positionId = application.Position.Id;
            this.messages.Add(new Message
            {
                Content = "From: " + application.Position.Company.Name + "<br />" + Message,
                UserId = application.User.Id,
            });
            this.applications.Delete(application);
            return Redirect("/company/position/" + positionId + "/applications");
        }

        [Route("position/{id}/apply")]
        [HttpGet]
        [IsUser]
        public ActionResult ApplyForPosition(int id)
        {
            this.applications.Add(new Application
            {
                PositionId = id,
                UserId = CurrentUser.Id,
            });
            return Redirect("/");
        }

        [Route("application/{id}/decline")]
        [HttpGet]
        [IsCompany]
        public ActionResult Decline(int id)
        {
            var application = this.applications.GetById(id);
            var positionId = application.Position.Id;
            this.applications.Delete(application);
            return Redirect("/company/position/" + positionId + "/applications");
        }

        [Route("company/position/{id}/applications")]
        [HttpGet]
        [IsCompany]
        public ActionResult ShowForPosition(int id)
        {
            var applications = this.applications.GetForPosition(id);
            return View(applications);
        }
    }
}