﻿using RayProfile.Data;
using RayProfile.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace RayProfile.Service
{
    public interface IUserService
    {/// <summary>
        /// Edit a user
        /// </summary>
        /// <param name="user">The user to be edited</param>
        void Edit(User user);

        /// <summary>
        /// Get all users
        /// </summary>
        /// <returns></returns>
        List<User> GetAll();

        /// <summary>
        /// Get top ten users with the higest rating
        /// </summary>
        /// <returns></returns>
        List<User> GetBest();

        /// <summary>
        /// Get a user by id
        /// </summary>
        /// <param name="id">The id of a user</param>
        /// <returns></returns>
        User GetById(int id);

        /// <summary>
        /// Gets user by given username
        /// </summary>
        /// <param name="username">Username</param>
        /// <returns></returns>
        User GetByName(string username);

        /// <summary>
        /// Registers user
        /// </summary>
        /// <param name="user">User</param>
        void Register(User user);

        /// <summary>
        /// Checks if user exists by given username
        /// </summary>
        /// <param name="username">Username</param>
        /// <returns></returns>
        bool UserExists(string username);

        /// <summary>
        /// Checks if given username or email exists
        /// </summary>
        /// <param name="username">Username</param>
        /// <param name="email">Email</param>
        /// <returns>If username or email exists</returns>
        bool UserOrEmailExists(string username, string email);
    }

    public class UserService : IUserService
    {
        private AppContext db = new AppContext();

        public void Edit(User user)
        {
            db.Entry(user).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public List<User> GetAll()
        {
            return db.Users.ToList();
        }

        public List<User> GetBest()
        {
            return db.Users
                .Include(x => x.UserVotes)
                .Where(x => x.CompanyVotes.Select(y => y.Value).Average() > 0)
                .OrderByDescending(x => x.UserVotes.Select(y => y.Value).Average())
                .ThenByDescending(x => x.UserVotes.Select(y => y.Value).Count())
                .Take(10)
                .ToList();
        }

        public User GetById(int id)
        {
            return db.Users.Find(id);
        }

        public User GetByName(string username)
        {
            return db.Users.First(x => x.Username == username);
        }

        public void Register(User user)
        {
            this.db.Users.Add(user);
            this.db.SaveChanges();
        }

        public bool UserExists(string username)
        {
            return db.Users.Any(x => x.Username == username);
        }

        public bool UserOrEmailExists(string username, string email)
        {
            return this.db.Users.Any(x => x.Email == email || x.Username == username);
        }
    }
}