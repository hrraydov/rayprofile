﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RayProfile.Models
{
    public class Company
    {
        private ICollection<CompanyVote> companyVotes;

        private ICollection<Position> positions;

        private ICollection<Product> products;

        private ICollection<UserVote> userVotes;

        public Company()
        {
            this.positions = new HashSet<Position>();
            this.products = new HashSet<Product>();
            this.companyVotes = new HashSet<CompanyVote>();
            this.userVotes = new HashSet<UserVote>();
        }

        public virtual ICollection<CompanyVote> CompanyVotes
        {
            get { return this.companyVotes; }
            set { this.companyVotes = value; }
        }

        public CompanyDetails Details { get; set; }

        [Required]
        [StringLength(200)]
        public string Email { get; set; }

        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(150)]
        public string LoginName { get; set; }

        [Required]
        [StringLength(200)]
        public string Name { get; set; }

        public virtual ICollection<Position> Offers
        {
            get { return this.positions; }
            set { this.positions = value; }
        }

        [Required]
        [StringLength(150)]
        public string Password { get; set; }

        public virtual ICollection<Product> Products
        {
            get { return this.products; }
            set { this.products = value; }
        }

        public virtual ICollection<UserVote> UserVotes
        {
            get { return this.userVotes; }
            set { this.userVotes = value; }
        }
    }
}