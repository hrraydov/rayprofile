﻿using System.ComponentModel.DataAnnotations;

namespace RayProfile.Web.ViewModels
{
    public class UserSkillsAddViewModel
    {
        [Required(
            ErrorMessageResourceName = "NameRequired",
            ErrorMessageResourceType = typeof(Resources.ViewModels.UserSkills)
            )]
        [StringLength(
            150,
            ErrorMessageResourceName = "NameLength",
            ErrorMessageResourceType = typeof(Resources.ViewModels.UserSkills)
            )]
        [Display(
            Name = "Name",
            ResourceType = typeof(Resources.ViewModels.UserSkills)
            )]
        public string Name { get; set; }

        public string SkillsJSON { get; set; }

        [Required(
            ErrorMessageResourceName = "ValueRequired",
            ErrorMessageResourceType = typeof(Resources.ViewModels.UserSkills)
            )]
        [Range(
            1,
            10,
            ErrorMessageResourceName = "ValueRange",
            ErrorMessageResourceType = typeof(Resources.ViewModels.UserSkills))]
        [Display(
            Name = "Value",
            ResourceType = typeof(Resources.ViewModels.UserSkills)
            )]
        public int Value { get; set; }
    }

    public class UserSkillsIndexViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Value { get; set; }
    }

    public class UserSkillsListViewModel
    {
        public string Name { get; set; }

        public int Value { get; set; }
    }
}