﻿using RayProfile.Models;
using RayProfile.Service;
using RayProfile.Web.Filters;
using RayProfile.Web.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace RayProfile.Web.Controllers
{
    public class UserSkillsController : BaseController
    {
        public UserSkillsController()
            : base()
        {
        }

        public UserSkillsController(ISkillService skillService)
        {
            this.skills = skillService;
        }

        [IsUser]
        [Route("user/skill/add")]
        [HttpGet]
        public ActionResult Add()
        {
            var viewModel = new UserSkillsAddViewModel();
            var skills = this.skills.GetAll();
            var skillNames = skills.Select(x => x.Name).ToList();
            var jsonSerializer = new JavaScriptSerializer();
            viewModel.SkillsJSON = jsonSerializer.Serialize(skillNames);
            viewModel.Value = 5;
            return View(viewModel);
        }

        [IsUser]
        [Route("user/skill/add")]
        [HttpPost]
        public ActionResult Add(UserSkillsAddViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                viewModel.Name = viewModel.Name.Trim();
                if (ModelState.IsValid)
                {
                    skills.AddToUser(new Skill
                    {
                        Name = viewModel.Name
                    }, CurrentUser.Id, viewModel.Value);
                    return Redirect("/user/skills");
                }
            }
            var jsonSerializer = new JavaScriptSerializer();
            viewModel.SkillsJSON = jsonSerializer.Serialize(this.skills.GetAll().Select(x => x.Name).ToList());
            return View(viewModel);
        }

        [IsUser]
        [Route("user/skill/delete/{id}")]
        [HttpGet]
        public ActionResult Delete(int id)
        {
            if (this.belongsToCurrentUser(id))
            {
                var skill = skills.Get(id);
                skills.RemoveFromUser(skill);
            }
            else
            {
                return Content("asd");
            }
            return Redirect("/user/skills");
        }

        [IsUser]
        [Route("user/skills")]
        [HttpGet]
        public ActionResult Index()
        {
            var skills = this.skills.GetByUserId(CurrentUser.Id);
            var viewModel = new List<UserSkillsIndexViewModel>();
            viewModel.AddRange(skills.Select(x => new UserSkillsIndexViewModel
            {
                Id = x.Id,
                Name = x.Skill.Name,
                Value = x.Value,
            }));
            return View(viewModel);
        }

        [HttpGet]
        [Route("api/skills/{userId}/{page?}/{pageSize?}")]
        public ActionResult List(int userId, int page = 1, int pageSize = 4)
        {
            var skills = this.skills.GetPagedByUserId(userId, page, pageSize);
            var viewModel = new List<UserSkillsListViewModel>();
            foreach (var item in skills)
            {
                viewModel.Add(new UserSkillsListViewModel
                {
                    Name = item.Skill.Name,
                    Value = item.Value,
                });
            }
            return Json(viewModel, JsonRequestBehavior.AllowGet);
        }

        private bool belongsToCurrentUser(int id)
        {
            return this.skills.BelongsTo(id, CurrentUser.Id);
        }
    }
}