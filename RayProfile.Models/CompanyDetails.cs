﻿using System.ComponentModel.DataAnnotations.Schema;

namespace RayProfile.Models
{
    [ComplexType]
    public class CompanyDetails
    {
        public string Desc { get; set; }

        public short? Founded { get; set; }

        public string Industry { get; set; }

        public string Site { get; set; }

        public CompanySize? Size { get; set; }
    }
}